jax==0.4.1
jaxlib==0.4.1

numpy==1.23.4
scipy==1.9.3
pandas
jupyter
jupyterlab

# Mac OS deps for tensorflow
#tensorflow-macos==2.10.0
#tensorflow==2.10.0
tensorboard==2.10.1
tensorflow-datasets==4.7.0

plotly

h5py~=3.7.0
tqdm~=4.64.1
nltk~=3.8