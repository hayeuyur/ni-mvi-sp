# NI-MVI: Semestral project

The report is in the `report.pdf`

# Installation

Before installation please update the `requirements.txt` file and select the tensorflow version depending on your OS.
If you are on Mac OS then uncomment `tensorflow-macos==2.10.0` and if you are on the linux uncomment `tensorflow==2.10.0`.
The project uses TensorFlow only for comparison of the recurrent neural networks and it uses `tensorflow-datasets` to 
load data. The rest is implemented using `JAX` library.
 
Then you can follow the 
[guide](https://datumorphism.leima.is/til/programming/python/python-anaconda-install-requirements/) to install deps using miniconda.

In the `benchmarks` directory you can find scripts and small python programs, which were created to measure the 
performance of ensembles. The scripts from `benchmarks/scrips` directory can be launched from the directory itself.

##### Some other small improvements

- [ ] Implement softplus
- [ ] Bidirectional RNN
- [ ] Implement early stopping
- [ ] Implement gradient skip
- [ ] Implement typing
- [ ] Implement transformer and positional embeddings
- [ ] Refactor RNN to use Linear Layer instead of the manual sum
- [ ] Implement tests