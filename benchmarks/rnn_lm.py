import argparse
import sys

import jax
import tensorflow as tf
from jax import random

# import jax.tools.colab_tpu
# jax.tools.colab_tpu.setup_tpu('tpu_driver_20221011')

sys.path.append('../')

from src.nn.layers import Sequential, Linear, OneCellRNN, Residual, Passthrough, Embedding, Broadcast
from src.nn.cells import RNNCell, LSTMCell, GRUCell

from src.nn.operators.unary import Linear as Identity, Sigmoid, Tanh, GeLU, Softmax, Swish

from src.nn.losses import CrossEntropyLoss
from src.nn.metrics import CategoricalAccuracy, Perplexity, BPC

from src.trainer.trainer import Trainer

from src.text import CharacterTokenizer, WordTokenizer
from src.text.utils import make_tokenizer_from_args

from src.cmd import CMDInterface

jax.config.update('jax_array', True)


class RNNInterface(CMDInterface):

    @classmethod
    def configure(cls, parser: argparse.ArgumentParser):
        Trainer.configure_argument_parser(parser)

        subparsers = parser.add_subparsers(help="Language modelling task. After fill you can specify learning")

        def chain(_parser):
            cls.add_default_configuration(_parser, sub_parsers=None)

        CharacterTokenizer.configure_argument_parser(parser, subparsers, update=chain)
        WordTokenizer.configure_argument_parser(parser, subparsers, update=chain)

    @staticmethod
    def make_tokenizer_from_parameters(args, key):
        return make_tokenizer_from_args(args, key=key)


def prepare(dataset,
            vocabulary_size,
            batch_size: int,
            shuffle_size: int = 5000,
            seed: int = 42):
    @tf.function
    def encode(sequence):
        input_text = sequence[:-1]
        target_text = sequence[1:]

        input_text = tf.one_hot(input_text, depth=vocabulary_size)
        target_text = tf.one_hot(target_text, depth=vocabulary_size)

        return input_text, target_text

    return dataset \
        .shuffle(shuffle_size, seed=seed) \
        .map(encode) \
        .batch(batch_size) \
        .prefetch(tf.data.AUTOTUNE)


def make_rnn(cell_type,
             hidden_state_dimension,
             embedding_dimension,
             intermediate_dimension,
             vocabulary_size):
    cell = None

    if cell_type == 'rnn':
        cell = RNNCell(input_shape=embedding_dimension,
                       output_shape=hidden_state_dimension,
                       activation=Tanh())

    if cell_type == 'lstm':
        cell = LSTMCell(input_shape=embedding_dimension,
                        output_shape=hidden_state_dimension,
                        activation=Sigmoid(),
                        recurrent_activation=Tanh())

    if cell_type == 'gru':
        cell = GRUCell(input_shape=embedding_dimension,
                       output_shape=hidden_state_dimension,
                       activation=Sigmoid(),
                       recurrent_activation=Tanh())

    return Sequential([
        # Embedding(vocabulary_size, output_shape=intermediate_dimension),
        Broadcast(
            Sequential([
                Linear(input_shape=vocabulary_size, output_shape=intermediate_dimension, activation=GeLU()),
                Linear(input_shape=intermediate_dimension, output_shape=embedding_dimension, activation=GeLU())
            ])
        ),
        OneCellRNN(cell=cell),
        Broadcast(
            Sequential([
                Linear(input_shape=hidden_state_dimension,
                       output_shape=intermediate_dimension,
                       activation=GeLU()),
                Linear(input_shape=intermediate_dimension,
                       output_shape=vocabulary_size,
                       activation=Softmax())
            ])
        )]
    )


def main(args: argparse.Namespace, interface: RNNInterface):
    key = random.PRNGKey(args.seed)
    tokenizer = interface.make_tokenizer_from_parameters(args, key=key)
    datasets, vocabulary = tokenizer.load(args)

    train, dev, test = datasets

    train = prepare(train, len(vocabulary), args.batch_size, 10000, args.seed)
    dev = prepare(dev, len(vocabulary), args.batch_size, 5000, args.seed)
    test = prepare(test, len(vocabulary), 1, 1, args.seed)

    initializer = interface.make_initializer_from_parameters(key, args)
    optimizer = interface.make_optimizer_from_parameters(args)

    model = make_rnn(
        args.cell,
        args.hidden_state_dimension,
        args.embedding_dimension,
        args.intermediate_dimension,
        len(vocabulary)
    )

    loss = CrossEntropyLoss(is_probability_distribution=True, sum_axis=2)

    trainer = Trainer(
        model,
        loss,
        optimizer=optimizer,
        initializer=initializer,
        metrics=[
            CategoricalAccuracy(is_probability_distribution=True),
            Perplexity(is_probability_distribution=True),
            BPC(is_probability_distribution=True)
        ]
    )

    state = trainer.train(train, dev, args)
    evaluation_info = trainer.evaluate(test, state, args, test=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--cell",
                        default='rnn',
                        type=str,
                        choices=['rnn', 'lstm', 'gru'],
                        help="The recurrent cell to use in the ")
    parser.add_argument("--hidden_state_dimension", default=32, type=int, help="Hidden state dimension")
    parser.add_argument("--embedding_dimension", default=16, type=int, help="Embedding dimension")
    parser.add_argument("--intermediate_dimension", default=64, type=int, help="Intermediate dimension")
    parser.add_argument("--max_learning_steps", default=-1, type=int, help="The value by which truncate the backpropagation")
    parser.add_argument("--batch_size", default=32, type=int, help="Batch size")
    parser.add_argument("--seed", default=42, type=int, help="Random seed")

    interface = RNNInterface()
    interface.configure(parser)

    args = parser.parse_args([] if "__file__" not in globals() else None)

    main(args, interface)
