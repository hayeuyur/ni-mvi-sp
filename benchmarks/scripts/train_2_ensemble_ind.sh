cd ..

mkdir -p models
mkdir -p logs

python ensemble_rnn_lm.py  --cell rnn \
                            --ensemble default \
                            --estimators 2 \
                            --alpha 0.0 \
                            --intermediate_dimension 96 \
                            --hidden_state_dimension 128 \
                            --embedding_dimension 64 \
                            --epochs 100 \
                            --batch_size 32 \
                            --output_model_path models/2_ensemble_rnn_ind.h5 \
                            --checkpoint 1 \
                            --reset_optimizer_state True \
                            --loss_file 2_ensemble_rnn_loss_ind.json \
                            --metrics_file 2_ensemble_rnn_metrics_ind.json \
                            CHAR Dataset --train_file data/ptb/ptb.train.txt \
                                         --test_file data/ptb/ptb.test.txt \
                                         --dev_file data/ptb/ptb.valid.txt \
                                         --vocabulary_size 128 \
                                         --sequence_size 256 \
                                         --window_size 128 \
                            INITIALIZER Xavier \
                            OPTIMIZER AdaBelief \
                            CLIP Norm --max_norm 10 \
                            SCHEDULER Constant --learning_rate 0.001