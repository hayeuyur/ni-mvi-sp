cd ..

mkdir -p models
mkdir -p logs

python ensemble_rnn_lm.py --cell rnn \
                            --ensemble default \
                            --estimators 4 \
                            --alpha 0.5 \
                            --intermediate_dimension 48 \
                            --hidden_state_dimension 96 \
                            --embedding_dimension 48 \
                            --epochs 100 \
                            --batch_size 32 \
                            --output_model_path models/4_ensemble_rnn.h5 \
                            --checkpoint 1 \
                            --reset_optimizer_state True \
                            --loss_file 4_ensemble_rnn_loss.json \
                            --metrics_file 4_ensemble_rnn_metrics.json \
                            CHAR Dataset --train_file data/ptb/ptb.train.txt \
                                         --test_file data/ptb/ptb.test.txt \
                                         --dev_file data/ptb/ptb.valid.txt \
                                         --vocabulary_size 128 \
                                         --sequence_size 256 \
                                         --window_size 128 \
                            INITIALIZER Xavier \
                            OPTIMIZER AdaBelief \
                            CLIP Norm --max_norm 10 \
                            SCHEDULER Constant --learning_rate 0.001