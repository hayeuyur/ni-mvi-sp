cd ..

mkdir -p models
mkdir -p logs

python rnn_lm.py --cell lstm \
                  --epochs 100 \
                  --batch_size 32 \
                  --hidden_state_dimension 128 \
                  --embedding_dimension 48 \
                  --intermediate_dimension 64 \
                  --output_model_path models/large_lstm.h5 \
                  --checkpoint 1 \
                  --reset_optimizer_state True \
                  --loss_file large_lstm_loss.json \
                  --metrics_file large_lstm_metrics.json \
                  CHAR Dataset --train_file data/ptb/ptb.train.txt \
                               --test_file data/ptb/ptb.test.txt \
                               --dev_file data/ptb/ptb.valid.txt \
                               --vocabulary_size 128 \
                               --sequence_size 256 \
                               --window_size 128 \
                  INITIALIZER Xavier \
                  OPTIMIZER AdaBelief \
                  CLIP Norm --max_norm 10 \
                  SCHEDULER Constant --learning_rate 0.001