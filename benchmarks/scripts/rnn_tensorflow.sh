 cd ..

mkdir -p models
mkdir -p logs

 python rnn_tf.py --epochs 100 \
                  --batch_size=32 \
                  --hidden_state_dimension 196 \
                  --embedding_dimension 64 \
                  --intermediate_dimension 128 \
                  CHAR Dataset --train_file data/ptb/ptb.train.txt \
                               --test_file data/ptb/ptb.test.txt \
                               --dev_file data/ptb/ptb.valid.txt \
                               --vocabulary_size 128 \
                               --sequence_size=256 \
                               --window_size=128 \
