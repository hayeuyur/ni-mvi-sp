cd ..

mkdir -p models
mkdir -p logs

python rnn_lm.py --cell rnn \
                  --epochs 100 \
                  --batch_size 32 \
                  --hidden_state_dimension 128 \
                  --embedding_dimension 64 \
                  --intermediate_dimension 96 \
                  --output_model_path models/test_rnn.h5 \
                  --checkpoint 1 \
                  --reset_optimizer_state True \
                  --loss_file test_rnn_loss.json \
                  --metrics_file test_rnn_metrics.json \
                  CHAR Dataset --train_file data/ptb/ptb.train.txt \
                               --test_file data/ptb/ptb.test.txt \
                               --dev_file data/ptb/ptb.valid.txt \
                               --vocabulary_size 128 \
                               --sequence_size 256 \
                               --window_size 128 \
                  INITIALIZER Xavier \
                  OPTIMIZER Adam \
                  CLIP Norm --max_norm 10 \
                  SCHEDULER Constant --learning_rate 0.001
