import sys
import tensorflow as tf
import argparse

from jax import random

sys.path.append('../')

from src.text import CharacterTokenizer, WordTokenizer
from src.text.utils import make_tokenizer_from_args


def prepare(dataset,
            vocabulary_size,
            batch_size: int,
            shuffle_size: int = 5000,
            seed: int = 42):
    @tf.function
    def encode(sequence):
        input_text = sequence[:-1]
        target_text = sequence[1:]

        input_text = tf.one_hot(input_text, depth=vocabulary_size)
        target_text = tf.one_hot(target_text, depth=vocabulary_size)

        return input_text, target_text

    return dataset \
        .shuffle(shuffle_size, seed=seed) \
        .map(encode) \
        .batch(batch_size) \
        .prefetch(tf.data.AUTOTUNE)


def main(args: argparse.Namespace):
    print(tf.config.list_physical_devices())
    tf.config.set_visible_devices([], 'GPU')

    tf.random.set_seed(args.seed)
    tf.config.threading.set_inter_op_parallelism_threads(1000)
    tf.config.threading.set_intra_op_parallelism_threads(1000)

    key = random.PRNGKey(args.seed)
    tokenizer = make_tokenizer_from_args(args, key=key)
    datasets, vocabulary = tokenizer.load(args)

    train, dev, test = datasets

    train = prepare(train, len(vocabulary), args.batch_size, 10000, args.seed)
    dev = prepare(dev, len(vocabulary), args.batch_size, 5000, args.seed)
    test = prepare(test, len(vocabulary), 1, 1000, args.seed)

    model = tf.keras.models.Sequential()

    model.add(tf.keras.layers.Dense(args.intermediate_dimension, activation=tf.keras.activations.gelu))
    model.add(tf.keras.layers.Dense(args.embedding_dimension, activation=tf.keras.activations.gelu))
    model.add(tf.keras.layers.SimpleRNN(args.hidden_state_dimension, return_sequences=True))
    model.add(tf.keras.layers.Dense(args.intermediate_dimension, activation=tf.keras.activations.gelu))
    model.add(tf.keras.layers.Dense(len(vocabulary), activation=tf.keras.activations.softmax))

    model.compile(
        optimizer=tf.optimizers.Adam(),
        loss=tf.losses.CategoricalCrossentropy(),
        metrics=[]
    )

    model.build(input_shape=next(iter(train))[0].shape)
    model.summary()

    model.fit(
        train,
        validation_data=dev,
        epochs=args.epochs
    )

    model.evaluate(test)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--cell",
                        default='rnn',
                        type=str,
                        choices=['rnn', 'lstm', 'gru'],
                        help="The recurrent cell to use in the ")
    parser.add_argument("--hidden_state_dimension", default=32, type=int, help="Hidden state dimension")
    parser.add_argument("--embedding_dimension", default=16, type=int, help="Embedding dimension")
    parser.add_argument("--intermediate_dimension", default=64, type=int, help="Intermediate dimension")
    parser.add_argument("--max_learning_steps", default=-1, type=int, help="The value by which truncate the backpropagation")
    parser.add_argument("--batch_size", default=32, type=int, help="Batch size")
    parser.add_argument("--epochs", default=100, type=int, help="Number of epochs")

    parser.add_argument("--seed", default=42, type=int, help="Random seed")

    subparsers = parser.add_subparsers(help="Language modelling task. After fill you can specify learning")

    CharacterTokenizer.configure_argument_parser(parser, subparsers)
    WordTokenizer.configure_argument_parser(parser, subparsers)

    args = parser.parse_args([] if "__file__" not in globals() else None)

    main(args)