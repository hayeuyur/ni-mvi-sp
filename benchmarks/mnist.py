import argparse
import sys

from functools import reduce

import jax.numpy as jnp
from jax import random
import jax

import tensorflow as tf
import tensorflow_datasets as tfds

sys.path.append('../')


from src.nn.layers import Linear, Sequential, Residual
from src.nn.operators.unary import Linear as Identity, GeLU, Tanh
from src.cmd import CMDInterface
from src.nn.losses.cross_entropy_loss import CrossEntropyLoss
from src.nn.metrics import CategoricalAccuracy, Perplexity

from src.trainer.trainer import Trainer

jax.config.update('jax_array', True)

class MNISTInterface(CMDInterface):
    @classmethod
    def configure(cls, parser: argparse.ArgumentParser):
        Trainer.configure_argument_parser(parser)

        subparsers = parser.add_subparsers(help="Optimizer configuration")
        cls.add_default_configuration(parser, sub_parsers=subparsers)


def make_dataset(key):
    datasets, info = tfds.load(
        'mnist',
        split=['train[:80%]', 'train[80%:]'],
        as_supervised=True,
        with_info=True
    )

    train, dev = datasets

    input_shape = jnp.array(info.features['image'].shape)
    input_shape = reduce(lambda result, x: result * x, input_shape, 1).tolist()

    labels = info.features['label']

    test = tfds.load(
        'mnist',
        split=['test'],
        shuffle_files=True,
        as_supervised=True,
    )[0]

    def prepare(image: tf.Tensor, label: tf.Tensor):
        image = tf.cast(image, dtype=tf.float32) / 255.
        label = tf.one_hot(label, depth=labels.num_classes)

        return tf.reshape(image, (input_shape,)), label

    datasets = [train, dev, test]
    train, test, dev = [dataset.map(prepare) for dataset in datasets]

    train = train \
        .shuffle(10000, seed=args.seed) \
        .batch(args.batch_size) \
        .prefetch(tf.data.AUTOTUNE)

    dev = dev \
        .shuffle(5000, seed=args.seed) \
        .batch(args.batch_size) \
        .prefetch(tf.data.AUTOTUNE)

    test = test.batch(1)

    return train, dev, test, input_shape, labels.num_classes


def main(args: argparse.Namespace, interface: MNISTInterface):
    key = random.PRNGKey(args.seed)

    train, dev, test, input_shape, labels = make_dataset(key)

    initializer = interface.make_initializer_from_parameters(key, args)
    optimizer = interface.make_optimizer_from_parameters(args)

    model = Sequential([
        Linear(input_shape, 512, activation=GeLU()),
        Linear(512, 256, activation=GeLU()),
        Residual(
            Sequential([Linear(256, 256, activation=GeLU()),
                        Linear(256, 256, activation=GeLU()),
                        Linear(256, 256, activation=GeLU())])
        ),
        Linear(256, 128, activation=Tanh()),
        Residual(
            Sequential([Linear(128, 128, activation=GeLU()),
                        Linear(128, 128, activation=GeLU()),
                        Linear(128, 128, activation=GeLU())])
        ),
        Linear(128, labels, activation=Identity()),
    ])

    loss = CrossEntropyLoss()

    trainer = Trainer(model,
                      loss=loss,
                      optimizer=optimizer,
                      initializer=initializer,
                      metrics=[CategoricalAccuracy(is_probability_distribution=False),
                               Perplexity(is_probability_distribution=False)])

    state = trainer.train(train=train, validation=dev, args=args)
    metric_info = trainer.evaluate(test, state)

    jax.block_until_ready(metric_info)

    print("Test metric info - {}".format(metric_info))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--batch_size", default=1024, type=int, help="Batch size")
    parser.add_argument("--seed", default=42, type=int, help="Random seed")

    interface = MNISTInterface()
    interface.configure(parser)

    args = parser.parse_args([] if "__file__" not in globals() else None)

    main(args, interface)
