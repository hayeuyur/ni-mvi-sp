import argparse
import sys

import jax
import tensorflow as tf
from jax import random

# import jax.tools.colab_tpu
# jax.tools.colab_tpu.setup_tpu('tpu_driver_20221011')

sys.path.append('../')

from src.nn.layers import Sequential, Linear, OneCellRNN, Passthrough, Apply, Broadcast, Reshape
from src.nn.cells import RNNCell, LSTMCell, GRUCell
from src.nn.metrics import CategoricalAccuracy, Perplexity, BPC

from src.nn.operators.unary import Linear as Identity, Sigmoid, Tanh, GeLU, Softmax, Mean

from src.ensemble import Ensemble
from src.ensemble.losses import NCLCrossEntropyLoss
from src.ensemble.metrics import NNMetric, NCLDiversity

from src.trainer import EnsembleTrainer

from src.text import CharacterTokenizer, WordTokenizer
from src.text.utils import make_tokenizer_from_args

from src.cmd import CMDInterface

jax.config.update('jax_array', True)


class EnsembleRNNInterface(CMDInterface):

    @classmethod
    def configure(cls, parser: argparse.ArgumentParser):
        EnsembleTrainer.configure_argument_parser(parser)

        subparsers = parser.add_subparsers(help="Language modelling task. After fill you can specify learning")

        def chain(_parser):
            cls.add_default_configuration(_parser, sub_parsers=None)

        CharacterTokenizer.configure_argument_parser(parser, subparsers, update=chain)
        WordTokenizer.configure_argument_parser(parser, subparsers, update=chain)

    @staticmethod
    def make_tokenizer_from_parameters(args, key):
        return make_tokenizer_from_args(args, key=key)


def prepare(dataset,
            vocabulary_size,
            batch_size: int,
            shuffle_size: int = 5000,
            seed: int = 42):
    @tf.function
    def encode(sequence):
        input_text = sequence[:-1]
        target_text = sequence[1:]

        input_text = tf.one_hot(input_text, depth=vocabulary_size)
        target_text = tf.one_hot(target_text, depth=vocabulary_size)

        return tf.cast(input_text, dtype=tf.int32), target_text

    return dataset \
        .shuffle(shuffle_size, seed=seed) \
        .map(encode) \
        .batch(batch_size) \
        .prefetch(tf.data.AUTOTUNE)


def make_base_rnn_model(cell_type,
                        hidden_state_dimension,
                        intermediate_dimension,
                        embedding_dimension,
                        input_size,
                        output_size,
                        include_softmax):
    cell = None

    if cell_type == 'rnn':
        cell = RNNCell(input_shape=embedding_dimension,
                       output_shape=hidden_state_dimension,
                       activation=Tanh())

    if cell_type == 'lstm':
        cell = LSTMCell(input_shape=embedding_dimension,
                        output_shape=hidden_state_dimension,
                        activation=Sigmoid(),
                        recurrent_activation=Tanh())

    if cell_type == 'gru':
        cell = GRUCell(input_shape=embedding_dimension,
                       output_shape=hidden_state_dimension,
                       activation=Sigmoid(),
                       recurrent_activation=Tanh())

    return Sequential([
        # Embedding(vocabulary_size, output_shape=intermediate_dimension),
        Broadcast(
            Sequential([
                Linear(input_shape=input_size, output_shape=intermediate_dimension, activation=GeLU()),
                Linear(input_shape=intermediate_dimension, output_shape=embedding_dimension, activation=GeLU())
            ])
        ),
        OneCellRNN(cell=cell),
        Broadcast(
            Sequential([
                Linear(input_shape=hidden_state_dimension,
                       output_shape=intermediate_dimension,
                       activation=GeLU()),
                Linear(input_shape=intermediate_dimension,
                       output_shape=output_size,
                       activation=Softmax() if include_softmax else Identity())
            ])
        )]
    )


def make_predictor_and_combine(input_shape, estimators, args):
    output_size = input_shape
    include_softmax = True

    predictor = Passthrough()
    combine = Apply(Mean(scale_grad=False))

    if args.ensemble == 'stacked_softmax':
        predictor = Broadcast(
            Sequential([
                Linear(input_shape=output_size,
                       output_shape=output_size,
                       activation=GeLU()),
                Linear(input_shape=output_size,
                       output_shape=output_size,
                       activation=Softmax())
            ])
        )

        flattened = estimators * output_size

        combine = Broadcast(
            Sequential([
                Reshape(input_shape=(estimators, output_size), output_shape=flattened),
                Linear(input_shape=flattened,
                       output_shape=output_size,
                       activation=GeLU()),
                Linear(input_shape=output_size,
                       output_shape=output_size,
                       activation=Softmax())
            ]),
            axis=1
        )

        include_softmax = False

    return predictor, combine, output_size, include_softmax


def make_ensemble(input_shape, args):
    estimators = args.estimators
    predictor, combine, output_size, include_softmax = make_predictor_and_combine(input_shape, estimators, args)

    model = make_base_rnn_model(
        args.cell,
        args.hidden_state_dimension,
        args.intermediate_dimension,
        args.embedding_dimension,
        input_shape,
        output_size=input_shape,
        include_softmax=True
    )

    return Ensemble(model, predictor, combine, estimators=estimators)


def main(args: argparse.Namespace, interface: EnsembleRNNInterface):
    key = random.PRNGKey(args.seed)
    tokenizer = interface.make_tokenizer_from_parameters(args, key=key)
    datasets, vocabulary = tokenizer.load(args)

    train, dev, test = datasets

    train = prepare(train, len(vocabulary), args.batch_size, 10000, args.seed)
    dev = prepare(dev, len(vocabulary), args.batch_size, 5000, args.seed)
    test = prepare(test, len(vocabulary), 1, 1000, args.seed)

    initializer = interface.make_initializer_from_parameters(key, args)
    optimizer = interface.make_optimizer_from_parameters(args)

    ensemble = make_ensemble(len(vocabulary), args)
    loss = NCLCrossEntropyLoss(is_probability_distribution=True, alpha=args.alpha, sum_axis=2)

    trainer = EnsembleTrainer(
        ensemble,
        loss,
        optimizer=optimizer,
        initializer=initializer,
        metrics=[
            NNMetric(CategoricalAccuracy(is_probability_distribution=True)),
            NNMetric(Perplexity(is_probability_distribution=True)),
            NNMetric(BPC(is_probability_distribution=True)),
            NCLDiversity(is_probability_distribution=True)
        ]
    )

    state = trainer.train(train, dev, args)
    evaluation_info = trainer.evaluate(test, state, args, test=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--cell", default='rnn', type=str, choices=['rnn', 'lstm', 'gru'],
                        help="The recurrent cell to use in the one-cell rnn")
    parser.add_argument("--ensemble", default='default', type=str,
                        choices=['default', 'stacked_softmax'],
                        help="""
                        The ensemble type. 
                        The `default` type means that each base model has its own softmax and the fusion is done by
                        averaging the results.
                        The `fuse_softmax` type means that each base model outputs compact encoding which is passed
                        to single fully-connected layer with softmax. The fusion of the results is done by averaging the
                        outputs of the models and passing them through global FC layer.
                        The `fuse_stacked_softmax` is the same as the `fuse_softmax`, but the outputs of the models are
                        flattened over axis and then passed through the softmax
                        """)
    parser.add_argument("--estimators", default=4, type=int, help='The number of ensemble estimators')
    parser.add_argument("--alpha", default=0.85, type=float, help='Regularization parameter')
    parser.add_argument("--hidden_state_dimension", default=64, type=int, help="Hidden state dimension")
    parser.add_argument("--intermediate_dimension", default=64, type=int, help="Intermediate dimension")
    parser.add_argument("--embedding_dimension", default=32, type=int, help="Embedding dimension")
    parser.add_argument(
        "--max_learning_steps", default=-1, type=int, help="The value by which truncate the backpropagation"
    )
    parser.add_argument("--batch_size", default=32, type=int, help="Batch size")
    parser.add_argument("--seed", default=42, type=int, help="Random seed")

    interface = EnsembleRNNInterface()
    interface.configure(parser)

    args = parser.parse_args([] if "__file__" not in globals() else None)

    main(args, interface)
