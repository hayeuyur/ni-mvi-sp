from abc import ABC, abstractmethod

from src.nn.optimizers.sgd import SGD
from src.nn.optimizers.momentum_sgd import MomentumSGD
from src.nn.optimizers import NesterovMomentumSGD
from src.nn.optimizers import AdaGrad
from src.nn.optimizers import RMSProp
from src.nn.optimizers.adam import Adam
from src.nn.optimizers.adabelief import AdaBelief

from src.nn.clip.no_clip import NoClip
from src.nn.clip.value_clip import ValueClip
from src.nn.clip.norm_clip import NormClip

from src.nn.initializers import Normal, Xavier

from src.nn.schedulers import ConstantScheduler, PolynomialScheduler, ExponentialScheduler, CosineScheduler

from argparse import ArgumentParser, Namespace

from .utils import add_to_arg_parser, make_value_from_parameters


def strip_end(text, suffix):
    # Remove suffix with the custom function to support Python 3.8
    if suffix and text.endswith(suffix):
        return text[:-len(suffix)]

    return text


optimizers = [SGD, MomentumSGD, NesterovMomentumSGD, AdaGrad, RMSProp, Adam, AdaBelief]
optimizer_ids = [optimizer.__name__ for optimizer in optimizers]

clips = [NoClip, ValueClip, NormClip]
clips_ids = [strip_end(clip.__name__, 'Clip') for clip in clips]

initializers = [Normal, Xavier]
initializer_id = [initializer.__name__ for initializer in initializers]

schedulers = [ConstantScheduler, PolynomialScheduler, ExponentialScheduler, CosineScheduler]
scheduler_id = [strip_end(scheduler.__name__, 'Scheduler') for scheduler in schedulers]

id_to_optimizer = dict(zip(optimizer_ids, optimizers))
id_to_clip = dict(zip(clips_ids, clips))
id_to_initializer = dict(zip(initializer_id, initializers))
id_to_scheduler = dict(zip(scheduler_id, schedulers))

optimizer_id_dest = "optimizer_id"
clip_id_dest = "clip_id"
initializer_id_dest = "initializer_id"
scheduler_id_dest = "scheduler_id_dest"

opt_prefix = "opt_"
clip_prefix = "clip_"
initializer_prefix = "init_"
scheduler_prefix = "sch_"


class CMDInterface(ABC):

    @abstractmethod
    def configure(cls, parser: ArgumentParser):
        pass

    @classmethod
    def add_default_configuration(cls, parser: ArgumentParser, sub_parsers):
        """
            Configure parser to allow inputs basic NN configuration from shell.
            """

        # Development note:
        #  To allow multiple commands, the configuration process must chain all possible combinations, because
        #  ArgumentParser doesn't support multiple subcommands natively. For example, the parsers are added
        #  in the following way.
        #    INITIALIZER -> OPTIMIZER + CLIP
        #    OPTIMIZER -> INITIALIZER -> CLIP
        #    OPTIMIZER -> INITIALIZER
        _subparsers = parser.add_subparsers(help="Configure options") if sub_parsers is None else sub_parsers

        cls.add_initializer(parser, subparsers=_subparsers, chain_optimizer=True)
        cls.add_optimizer(parser, subparsers=_subparsers, chain_initializer=True)

    @classmethod
    def add_initializer(cls, parser: ArgumentParser, subparsers, chain_optimizer=False):
        _subparsers = parser.add_subparsers(help="Configure initializer") if subparsers is None else subparsers

        add_to_arg_parser(
            _subparsers,
            parent_parsers=[],
            command_name="INITIALIZER",
            destination_id=initializer_id_dest,
            param_prefix=initializer_prefix,
            items=id_to_initializer,
            skip_params=["key"],
            update=lambda _parser: cls.add_optimizer(_parser, None, chain_initializer=False) if chain_optimizer else None
        )

    @classmethod
    def add_optimizer(cls, _parser: ArgumentParser, subparsers, chain_initializer=False):
        _subparsers = _parser.add_subparsers(help="Configure optimizer") if subparsers is None else subparsers

        def __update__(_parser):
            __subparsers = _parser.add_subparsers(help="Configure optimizer")

            cls.add_clipping(_parser, __subparsers, chain_initializer=chain_initializer, chain_scheduler=True)
            cls.add_scheduler(_parser, __subparsers, chain_initializer=chain_initializer, chain_clipping=True)

            if chain_initializer:
                cls.add_initializer(_parser, __subparsers, chain_optimizer=False)

        add_to_arg_parser(_subparsers,
                          parent_parsers=[],
                          command_name="OPTIMIZER",
                          destination_id=optimizer_id_dest,
                          param_prefix=opt_prefix,
                          items=id_to_optimizer,
                          skip_params=["clip", "scheduler"],
                          update=__update__)

    @classmethod
    def add_clipping(cls, optimizer_parser: ArgumentParser, subparsers, chain_initializer=False, chain_scheduler=False):
        _subparsers = optimizer_parser.add_subparsers(
            help="Configure clipping method") if subparsers is None else subparsers

        def __update__(_parser):
            if chain_initializer or chain_scheduler:
                __subparsers = _parser.add_subparsers(help="Configure optimizer")

                if chain_initializer:
                    cls.add_initializer(_parser, __subparsers, chain_optimizer=False)

                if chain_scheduler:
                    cls.add_scheduler(_parser, __subparsers, chain_clipping=False)

        add_to_arg_parser(
            _subparsers,
            parent_parsers=[],
            command_name="CLIP",
            destination_id=clip_id_dest,
            param_prefix=clip_prefix,
            items=id_to_clip,
            update=__update__)

    @classmethod
    def add_scheduler(cls, optimizer_parser: ArgumentParser, subparsers, chain_initializer=False, chain_clipping=False):
        _subparsers = optimizer_parser.add_subparsers(
            help="Configure scheduler") if subparsers is None else subparsers

        def __update__(_parser):
            if chain_initializer or chain_clipping:
                __subparsers = _parser.add_subparsers(help="Configure optimizer")

                if chain_initializer:
                    cls.add_initializer(_parser, __subparsers, chain_optimizer=False)

                if chain_clipping:
                    cls.add_clipping(_parser, __subparsers, chain_initializer=False, chain_scheduler=False)

        add_to_arg_parser(
            _subparsers,
            parent_parsers=[],
            command_name="SCHEDULER",
            destination_id=scheduler_id_dest,
            param_prefix=scheduler_prefix,
            items=id_to_scheduler,
            update=__update__)

    @staticmethod
    def make_optimizer_from_parameters(args: Namespace):
        clip = make_value_from_parameters(args, clip_id_dest, id_to_clip, clip_prefix)

        if clip is None:
            clip = NoClip()

        scheduler = make_value_from_parameters(args, scheduler_id_dest, id_to_scheduler, scheduler_prefix)

        if scheduler is None:
            scheduler = ConstantScheduler()

        return make_value_from_parameters(args,
                                          optimizer_id_dest,
                                          id_to_optimizer,
                                          opt_prefix,
                                          clip=clip,
                                          scheduler=scheduler)

    @staticmethod
    def make_initializer_from_parameters(key, args: Namespace):
        initializer = make_value_from_parameters(args, initializer_id_dest, id_to_initializer, initializer_prefix, key=key)

        if initializer is None:
            initializer = Normal(key)

        return initializer
