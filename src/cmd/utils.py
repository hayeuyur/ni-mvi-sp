
from argparse import ArgumentParser, Namespace
from typing import Dict, Any, Callable, List
from src.utils.parameters import get_parameters


def add_to_arg_parser(sub_parsers,
                      parent_parsers: List[Any] = [],
                      command_name: str = "",
                      destination_id: str = "",
                      param_prefix: str = "",
                      items: Dict[str, Any] = {},
                      skip_params: List[str] = [],
                      update: Callable[[ArgumentParser], None] = None):
    parser = sub_parsers.add_parser(command_name)
    sub_parsers = parser.add_subparsers(dest=destination_id)

    for id, item_class in items.items():
        parser = sub_parsers.add_parser(id, parents=parent_parsers)

        for parameter in get_parameters(item_class.__init__, include_self=False):
            if parameter.name not in skip_params:
                parser.add_argument(
                    "--{}".format(parameter.name),
                    default=parameter.default,
                    type=parameter.type,
                    help="Type: {}, Default: {}".format(parameter.type, parameter.default),
                    dest=param_prefix + parameter.name)

        if update is not None:
            update(parser)


def make_value_from_parameters(args: Namespace,
                               id: str,
                               id_to_class: Dict[str, Any],
                               param_predix: str = "",
                               **kwargs):
    class_id = args.__dict__.get(id)

    if class_id is None:
        return None

    value_class = id_to_class[class_id]

    parameters = {
        p.name: args.__dict__[param_predix + p.name] for p in get_parameters(value_class.__init__)
        if param_predix + p.name in args
    }

    return value_class(**parameters, **kwargs)
