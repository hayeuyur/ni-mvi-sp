from argparse import ArgumentParser, Namespace

from abc import ABC, abstractmethod
from .utils import configure, make_configuration_from_args, FileConfiguration, DatasetConfiguration

from src.utils.random import generate_indices


class Tokenizer(ABC):

    def __init__(self, key):
        self.key = key

    def __make_data_from__file__(self, configuration: FileConfiguration, args: Namespace):
        text = self.__load_file__(configuration.file)

        params = self.__make_sequence_and_window_size__(is_evaluation=False, args=args)
        windows = self.__generate_windows__(text, *params)

        key, train_indices, dev_indices, test_indices = generate_indices(
            len(windows), configuration.train_size, configuration.dev_size, configuration.test_size, self.key
        )

        self.key = key

        datasets = [[windows[index] for index in indices] for indices in [train_indices, dev_indices, test_indices]]

        vocabulary = self.__default_vocabulary__()
        vocabulary = self.__fill_vocabulary__(datasets[0], vocabulary, vocabulary_size=args.vocabulary_size)

        datasets = self.__encode__(datasets, vocabulary)

        return datasets, vocabulary

    def __make_data_from_dataset__(self, configuration: DatasetConfiguration, args: Namespace):
        train = self.__load_file__(configuration.train_file)
        test = self.__load_file__(configuration.test_file)
        validation = self.__load_file__(configuration.dev_file)

        train_parameters = self.__make_sequence_and_window_size__(is_evaluation=False, args=args)
        validation_parameters = self.__make_sequence_and_window_size__(is_evaluation=True, args=args)

        train = self.__generate_windows__(train, *train_parameters)
        test = self.__generate_windows__(test, *validation_parameters)
        validation = self.__generate_windows__(validation, *validation_parameters)

        datasets = [train, test, validation]

        vocabulary = self.__default_vocabulary__()
        vocabulary = self.__fill_vocabulary__(datasets[0], vocabulary, vocabulary_size=args.vocabulary_size)

        datasets = self.__encode__(datasets, vocabulary)

        return datasets, vocabulary

    @classmethod
    @abstractmethod
    def __generate_windows__(cls, text: str, sequence_size, shift):
        pass

    @staticmethod
    @abstractmethod
    def __tokenize__(ngrams, vocabulary):
        pass

    @staticmethod
    @abstractmethod
    def __load_file__(file: str):
        pass

    @staticmethod
    @abstractmethod
    def __fill_vocabulary__(ngrams, vocabulary, vocabulary_size):
        pass

    @abstractmethod
    def __encode__(self, datasets, vocabulary):
        pass

    @staticmethod
    @abstractmethod
    def __make_sequence_and_window_size__(is_evaluation: bool, args: Namespace):
       pass

    @staticmethod
    @abstractmethod
    def __default_vocabulary__():
        pass
