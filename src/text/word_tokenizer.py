
from collections import defaultdict

from argparse import ArgumentParser, Namespace
from .utils import configure, make_configuration_from_args, FileConfiguration, DatasetConfiguration

import tensorflow as tf

from .tokenizer import Tokenizer


class WordTokenizer(Tokenizer):
    unknown_token: str = "<unk>"
    eos_token: str = "<eos>"

    def __init__(self, key):
        super(WordTokenizer, self).__init__(key)

    @staticmethod
    def configure_argument_parser(parser: ArgumentParser, subparsers, update=None):
        def add_args(_parser):
            _parser.add_argument("--vocabulary_size", default=128, type=int,
                                 help="The size of the character vocabulary")
            _parser.add_argument("--sequence_size", default=128, type=int, help="The size of sequence size")
            _parser.add_argument("--window_size",
                                 default=None,
                                 type=int,
                                 help="The size of the sliding window. If none, then use half of window")

            if update is not None:
                update(_parser)

        configure(parser, subparsers, "WORD", add_args)

    def load(self, args: Namespace):
        configuration = make_configuration_from_args(args)

        if isinstance(configuration, FileConfiguration):
            return self.__make_data_from__file__(configuration, args)

        if isinstance(configuration, DatasetConfiguration):
            return self.__make_data_from_dataset__(configuration, args)

        return None

    @classmethod
    def __generate_windows__(cls, text: str, sequence_size, shift):
        words = []

        for line in text.split('\n'):
            for word in line.split():
                words += [word]

            words += [cls.eos_token]

        return [words[index:index + sequence_size] for index in range(0, len(words) - sequence_size + 1, shift)]

    @classmethod
    def __fill_vocabulary__(cls, sequences, vocabulary, vocabulary_size):
        occurrences = defaultdict(lambda: 0)

        for sequence in sequences:
            for word in sequence:
                if word not in vocabulary.values():
                    occurrences[word] += 1

        top_list = sorted(zip(occurrences.keys(), occurrences.values()), key=lambda x: x[1], reverse=True)

        print(f"Found {len(top_list)} unique words")

        new_vocabulary = vocabulary

        for key, value in top_list:
            new_vocabulary[key] = len(new_vocabulary)

            if len(new_vocabulary) > vocabulary_size:
                break

        return new_vocabulary

    @staticmethod
    def __tokenize__(sequence, vocabulary):
        return [[vocabulary.get(word, 0) for word in sequence] for sequence in sequence]

    @staticmethod
    def __make_sequence_and_window_size__(is_evaluation: bool, args: Namespace):
        sequence_size = args.sequence_size

        shift = None

        if is_evaluation:
            shift = args.sequence_size
        else:
            shift = args.window_size if args.window_size is not None else int(sequence_size / 2)

        return sequence_size, shift

    @staticmethod
    def __load_file__(file: str):
        text = None

        with open(file, 'r') as f:
            text = f.read()

        return text

    def __encode__(self, datasets, vocabulary):
        datasets = [self.__tokenize__(df, vocabulary) for df in datasets]
        datasets = [tf.data.Dataset.from_tensor_slices(df) for df in datasets]

        return datasets

    @classmethod
    def __default_vocabulary__(cls):
        table = defaultdict(lambda: 0)

        table[0] = "<unk>"
        table[1] = cls.eos_token

        return table
