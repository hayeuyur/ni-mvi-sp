import argparse
import tensorflow as tf

from collections import defaultdict

from argparse import ArgumentParser, Namespace
from .utils import configure, make_configuration_from_args, FileConfiguration, DatasetConfiguration

from .tokenizer import Tokenizer


class CharacterTokenizer(Tokenizer):

    def __init__(self, key):
        super(CharacterTokenizer, self).__init__(key)

    def load(self, args: Namespace):
        configuration = make_configuration_from_args(args)

        if isinstance(configuration, FileConfiguration):
            return self.__make_data_from__file__(configuration, args)

        if isinstance(configuration, DatasetConfiguration):
            return self.__make_data_from_dataset__(configuration, args)

        return None

    @staticmethod
    def configure_argument_parser(parser: ArgumentParser, subparsers, update=None):
        def add_args(_parser):
            _parser.add_argument("--vocabulary_size", default=128, type=int,
                                 help="The size of the character vocabulary")
            _parser.add_argument("--sequence_size", default=128, type=int, help="The size of sequence size")
            _parser.add_argument("--window_size",
                                 default=None,
                                 type=int,
                                 help="The size of the sliding window. If none, then use half of window")

            if update is not None:
                update(_parser)

        configure(parser, subparsers, "CHAR", add_args)

    @staticmethod
    def __tokenize__(ngrams, vocabulary):
        return [[vocabulary.get(char, 0) for char in ngram] for ngram in ngrams]

    @staticmethod
    def __load_file__(file: str):
        text = None

        with open(file, 'r', encoding='utf8') as f:
            text = f.read()

        return text

    @classmethod
    def __generate_windows__(cls, text, sequence_size, shift):
        return [text[index:index + sequence_size] for index in range(0, len(text) - sequence_size + 1, shift)]

    @staticmethod
    def __make_sequence_and_window_size__(is_evaluation: bool, args: argparse.Namespace):
        sequence_size = args.sequence_size
        shift = args.window_size if args.window_size is not None else int(sequence_size / 2)

        return sequence_size, shift

    @staticmethod
    def __fill_vocabulary__(ngrams, vocabulary, vocabulary_size):
        occurrences = defaultdict(lambda: 0)

        for ngram in ngrams:
            for token in ngram:
                occurrences[token] += 1

        top_list = sorted(zip(occurrences.keys(), occurrences.values()), key=lambda x: x[1], reverse=True)

        new_vocabulary = vocabulary

        for key, value in top_list:
            new_vocabulary[key] = len(new_vocabulary)

            if len(new_vocabulary) == vocabulary_size:
                break

        return new_vocabulary

    def __encode__(self, datasets, vocabulary):
        datasets = [self.__tokenize__(df, vocabulary) for df in datasets]
        datasets = [tf.data.Dataset.from_tensor_slices(df) for df in datasets]

        return datasets

    @staticmethod
    def __default_vocabulary__():
        table = defaultdict(lambda: 0)
        table[0] = "<UNK>"

        return table
