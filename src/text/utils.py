
from argparse import ArgumentParser, Namespace
from src.cmd.utils import add_to_arg_parser, make_value_from_parameters


class FileConfiguration:

    def __init__(self, file: str, train_size: float = 0.75, test_size: float = 0.15, dev_size: float = 0.1):
        self.file = file
        self.train_size = train_size
        self.test_size = test_size
        self.dev_size = dev_size


class DatasetConfiguration:

    def __init__(self, train_file: str, dev_file: str, test_file: str):
        self.train_file = train_file
        self.dev_file = dev_file
        self.test_file = test_file


configurations = [FileConfiguration, DatasetConfiguration]
configurations_ids = ["File", "Dataset"]
configuration_id_dest = "input_configuration_id"
configuration_prefix = "in_cnf_"
id_to_configuration = dict(zip(configurations_ids, configurations))
tokenizer_type_key = "tokenizer_type"


def configure(parser: ArgumentParser, subparsers, command_name, update):

    def add_args(_parser):
        update(_parser)

    subparsers.dest = tokenizer_type_key

    add_to_arg_parser(subparsers,
                      parent_parsers=[],
                      command_name=command_name,
                      destination_id=configuration_id_dest,
                      param_prefix=configuration_prefix,
                      items=id_to_configuration,
                      skip_params=[],
                      update=add_args if update is not None else None)


def make_configuration_from_args(args: Namespace):
    return make_value_from_parameters(args, configuration_id_dest, id_to_configuration, configuration_prefix)


def make_tokenizer_from_args(args, **kwargs):
    from .character_tokenizer import CharacterTokenizer
    from .word_tokenizer import WordTokenizer

    key = getattr(args, tokenizer_type_key)

    if key == 'CHAR':
        return CharacterTokenizer(**kwargs)

    if key == 'WORD':
        return WordTokenizer(**kwargs)


    return None

