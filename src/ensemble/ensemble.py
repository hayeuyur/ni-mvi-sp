import jax
import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class, tree_map

from src.nn.layers import Layer, LayerState, LayerGradState, LayerInput, LayerOutput
from src.utils.tree import tree_map_variables

from functools import partial


base_predictions_key: str = "base_predictions"


class State(LayerState):
    base_model_states: LayerState
    predictor_states: LayerState
    combine_predictor_state: LayerState


class GradState(LayerGradState):
    base_model_grad_states: LayerGradState
    predictor_grad_states: LayerGradState
    combine_predictor_model_grad_state: LayerGradState


@register_pytree_node_class
class Ensemble(Layer):
    """
    Simple ensemble model with interpolation between joint training and training with shared softmax layer
    """

    def __init__(self,
                 base_model: Layer,
                 predictor_model: Layer,
                 combine_predictor_model: Layer,
                 estimators: int):
        """
        :param base_model: The base model to process input
        :param predictor_model: The model to perform predictions from the output of the base model
        :param combine_predictor_model: The model to combine outputs of each model from the stack and perform
                                        predictions
        :param estimators: The number of the estimators
        """
        super(Ensemble, self).__init__()

        self.base_model = base_model
        self.predictor_model = predictor_model
        self.combine_predictor_model = combine_predictor_model
        self.estimators = estimators

    def new_state(self):
        base_model_states = tree_map_variables(
            lambda x: jnp.repeat(x[jnp.newaxis, :], self.estimators, axis=0), self.base_model.new_state()
        )
        predictor_states = tree_map_variables(
            lambda x: jnp.repeat(x[jnp.newaxis, :], self.estimators, axis=0), self.predictor_model.new_state()
        )
        combine_predictor_model = self.combine_predictor_model.new_state()

        return State(base_model_states, predictor_states, combine_predictor_model)

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: State) -> LayerOutput:
        """
        forward pass through ensemble

        :param inputs: Inputs to get the model data from
        :param grad: If true will compute grad state
        :param state: The state of the ensemble
        :return:
        """
        def forward_model(_state):
            return self.base_model.forward(inputs, grad, _state)

        def forward_predictor(out, _state):
            _values = LayerInput(out, additional_info=())

            return self.predictor_model.forward(_values, grad, _state)

        forwarded: LayerOutput = jax.vmap(forward_model)(state.base_model_states)
        predictions: LayerOutput = jax.vmap(forward_predictor)(forwarded.out, state.predictor_states)

        values = LayerInput(forwarded.out, additional_info=())
        combined = self.combine_predictor_model.forward(values, grad, state.combine_predictor_state)

        grad_states = GradState(forwarded.grad_state, predictions.grad_state, combined.grad_state)

        return LayerOutput(out=combined.out,
                           grad_state=grad_states,
                           additional_info={base_predictions_key: forwarded.out})

    @jit
    def __backward__(self,
                     gradient: LayerInput,
                     state: State,
                     grad_state: GradState) -> LayerOutput:
        """
        Backward pass through ensemble

        :param gradient: The gradient, where at in parameter, where `input` is the gradient through combined layer and
                        `self.base_predictions_key` are gradients through predictor
        :param state: The state of the ensemble
        :param grad_state: The gradient state of the ensemble
        :return:
        """
        combine_grad = gradient.input
        predictor_grads = gradient.additional_info[base_predictions_key]

        combine_model_out = self.combine_predictor_model.backward(
            LayerInput(input=combine_grad, additional_info=dict()),
            state.combine_predictor_state,
            grad_state.combine_predictor_model_grad_state
        )

        @jit
        def __predictor_backward__(grad, _state, _grad_state):
            value = LayerInput(grad, additional_info={})

            return self.predictor_model.backward(value, _state, _grad_state)

        @jit
        def __base_backward__(grad, _state, _grad_state):
            value = LayerInput(grad, additional_info={})

            return self.base_model.backward(value, _state, _grad_state)

        predictor_models_out: LayerOutput = jax.vmap(
            __predictor_backward__
        )(predictor_grads, state.predictor_states, grad_state.predictor_grad_states)

        # Sum grads
        base_model_grads = predictor_models_out.out + combine_model_out.out

        base_model_out: LayerOutput = jax.vmap(
            __base_backward__
        )(base_model_grads, state.base_model_states, grad_state.base_model_grad_states)

        grads = State(base_model_out.grad_state,
                      predictor_models_out.grad_state,
                      combine_model_out.grad_state)

        return LayerOutput(base_model_out.out, grads, additional_info=dict())

    def tree_flatten(self):
        children = (self.base_model, self.predictor_model, self.combine_predictor_model)
        aux_data = {"estimators": self.estimators}

        return children, aux_data
