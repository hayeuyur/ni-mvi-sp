import jax
import jax.numpy as jnp

from jax import jit

from .emsemble_metric import EnsembleMetric, EnsembleMetricState
from src.nn.metrics import Metric, MetricState

from jax.tree_util import register_pytree_node_class


class State(EnsembleMetricState):
    metric: MetricState
    base_predictor_metrics: MetricState

    def evaluate(self):
        metric_evaluation = self.metric.evaluate()
        base_metric_evaluation = jax.vmap(lambda state: state.evaluate())(self.base_predictor_metrics)

        return dict(metric_evaluation=metric_evaluation, base_metric_evaluation=base_metric_evaluation)


@register_pytree_node_class
class NNMetric(EnsembleMetric):
    """
    A simple wrapper around metric from nn layer
    """

    def __init__(self, base_metric: Metric):
        """
        :param base_metric: the instance of the base metric
        """
        super(NNMetric, self).__init__(name=base_metric.name)

        self.base_metric = base_metric

    def new_state(self, estimators: int = 0) -> State:
        metric_state = self.base_metric.new_state()
        base_estimator_states = jax.vmap(lambda _: self.base_metric.new_state())(jnp.arange(estimators))

        return State(metric_state, base_estimator_states)

    @jit
    def forward(self, predictions, base_predictions, targets, state: State):
        new_metric_state = self.base_metric.forward(predictions, targets, state.metric)

        new_base_metric_states = jax.vmap(
            lambda _predictions, _state: self.base_metric.forward(_predictions, targets, _state),
            in_axes=(1, 0)
        )(base_predictions, state.base_predictor_metrics)

        return State(new_metric_state, new_base_metric_states)

    def tree_flatten(self):
        children = (self.base_metric,)
        aux_data = {}

        return children, aux_data

