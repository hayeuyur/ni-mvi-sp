import jax
import jax.numpy as jnp

from jax import jit
from jax.tree_util import register_pytree_node_class

from src.utils.utils import softmax
from .emsemble_metric import EnsembleMetric, EnsembleMetricState


class State(EnsembleMetricState):
    diversities: jnp.ndarray

    def evaluate(self):
        return jnp.mean(self.diversities, axis=-1)


@register_pytree_node_class
class NCLDiversity(EnsembleMetric):
    """
    The diversity metric from the Generalized Negative correlation learning from the
    """

    def __init__(self, axis: int = -1, is_probability_distribution: bool = False, name: str = "Diversity"):
        super(NCLDiversity, self).__init__(name)

        self.axis = axis
        self.is_probability_distribution = is_probability_distribution
        self.name = name

    @staticmethod
    def new_state(estimators: int = 0) -> State:
        return State(jnp.array([]).reshape(0, estimators))

    @jit
    def forward(self, predictions, base_predictions, targets, state):
        if not self.is_probability_distribution:
            predictions = softmax(predictions, axis=self.axis)
            base_predictions = softmax(base_predictions, axis=self.axis)

        def compute_diversity(_predictions, _base_predictions):
            hessian = jnp.eye(_predictions.shape[self.axis])

            hessian -= _predictions.T
            hessian *= _predictions

            difference = _base_predictions - _predictions

            return difference @ hessian @ difference

        compute_diversity_vc = compute_diversity

        # Broadcast all axes which
        for _ in range(1, len(predictions.shape) - 1):
            compute_diversity_vc = jax.vmap(
                compute_diversity_vc,
                in_axes=(0, 0),
                out_axes=0
            )

        diversities = jax.vmap(
            jax.vmap(
                compute_diversity_vc,
                in_axes=(None, 0),
                out_axes=0
            ),
            in_axes=(0, 0),
            out_axes=0
        )(predictions, base_predictions)

        for _ in range(1, len(predictions.shape) - 1):
            diversities = jnp.mean(diversities, axis=-1)

        return State(jnp.vstack([state.diversities, diversities]))
