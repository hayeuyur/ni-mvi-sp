from abc import ABC, abstractmethod
from jax import jit

from typing import NamedTuple

from src.utils.jit import PytreeObject


EnsembleMetricState = NamedTuple


class EnsembleMetric(PytreeObject, ABC):

    def __init__(self, name):
        self.name = name

    @abstractmethod
    @jit
    def forward(self, predictions, base_predictions, targets, state):
        pass

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
