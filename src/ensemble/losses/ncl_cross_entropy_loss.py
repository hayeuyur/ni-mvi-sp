import jax

from src.ensemble.losses.ensemble_loss import EnsembleLoss, Result
from src.utils.utils import softmax, mean_sparse_crossentropy_loss, sparse_crossentropy_loss_gradient

import jax.numpy as jnp

from jax import jit
from jax.tree_util import register_pytree_node_class


@register_pytree_node_class
class NCLCrossEntropyLoss(EnsembleLoss):
    """
    Ensemble loss function to maintain diversity proposed in the "Generalized Negative Correlation
    Learning for Deep Ensembling" from Sebastian Buschjäger, Lukas Pfahler, Katharina Morik
    """

    def __init__(self,
                 is_probability_distribution: bool = False,
                 alpha: float = 0.0,
                 axis=-1,
                 sum_axis=-1):
        """
        :param alpha: the interpolation ratio between independent model training and the end to end training,
                      if alpha = 1, then all models are trained end to end
        :param axis: The axis of probability distribution
        :param sum_axis: The axis to perform sum over elements. For example, in the RNN we want to minimize
                         sum of the sparse cross entropy loss across sequence. Therefore, we want to perform
                         double sum over the sequence and over distribution
        """
        self.is_probability_distribution = is_probability_distribution
        self.alpha = alpha
        self.axis = axis
        self.sum_axis = sum_axis

    @jit
    def evaluate(self, predictions, base_predictions, targets, grad: bool = True):
        if not self.is_probability_distribution:
            predictions = softmax(predictions, axis=self.axis)

        grad = sparse_crossentropy_loss_gradient(predictions, targets)
        loss = mean_sparse_crossentropy_loss(predictions, targets, axis=self.sum_axis)

        def __evaluate__(predictions):
            _predictions = jax.lax.cond(self.is_probability_distribution,
                                        lambda x: x,
                                        lambda x: softmax(predictions, axis=self.axis),
                                        predictions)

            _loss = mean_sparse_crossentropy_loss(_predictions, targets, axis=self.sum_axis)
            gradient = sparse_crossentropy_loss_gradient(_predictions, targets)

            return _loss, gradient

        base_predictions_losses, base_predictions_grads = jax.vmap(
            __evaluate__, in_axes=1, out_axes=(0, 1)
        )(base_predictions)

        independent_loss = 1. / base_predictions_losses.size * jnp.sum(base_predictions_losses)

        result = Result(
            loss=self.alpha * loss + (1. - self.alpha) * independent_loss,
            fused_loss=loss,
            independent_loss=independent_loss,
            base_predictor_losses=base_predictions_losses,
            grad=self.alpha * grad,
            base_predictor_grad=(1. - self.alpha) / base_predictions_losses.size * base_predictions_grads
        )

        return result
