from abc import ABC, abstractmethod

from jax import jit
import jax.numpy as jnp

from typing import NamedTuple, List
from src.utils.jit import PytreeObject


class Result(NamedTuple):
    loss: float
    fused_loss: float
    independent_loss: float
    base_predictor_losses: List[float]
    grad: jnp.ndarray
    base_predictor_grad: jnp.ndarray


class EnsembleLoss(PytreeObject, ABC):

    @abstractmethod
    @jit
    def evaluate(self, predictions, base_predictions, targets, grad: bool = True) -> Result:
        pass

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
