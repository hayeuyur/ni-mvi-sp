from abc import ABC, abstractmethod
from src.nn.layers import LayerInput


class Converter(ABC):
    """
    Simple wrapper to map pytree to LayerInput
    """

    @staticmethod
    @abstractmethod
    def convert(inputs) -> LayerInput:
        pass


class DefaultConverter:

    @staticmethod
    def convert(inputs) -> LayerInput:
        return LayerInput(inputs, additional_info=dict())
