import os
import json

from abc import ABC, abstractmethod
from tqdm import tqdm

import jax
import jax.numpy as jnp
from jax.tree_util import tree_structure
from jax.tree_util import register_pytree_node_class

from collections import OrderedDict

from src.utils.tensorflow_support import from_tensorflow_to_jax

from src.utils.jit import PytreeObject
from src.utils.h5py import load_tree, write_tree

from .converter import Converter, DefaultConverter


@register_pytree_node_class
class BaseTrainer(PytreeObject, ABC):

    def __init__(self, model, loss, metrics, initializer, optimizer, converter: Converter = DefaultConverter()):
        self.model = model
        self.loss = loss
        self.metrics = metrics
        self.initializer = initializer
        self.optimizer = optimizer
        self.converter = converter

    @abstractmethod
    def __new_state__(self):
        pass

    @abstractmethod
    def __make_new_state__(self, *params):
        pass

    @abstractmethod
    def __new_metric_states__(self):
        pass

    @abstractmethod
    def __print_info__(self, iterator, loss, new_lr):
        pass

    @abstractmethod
    def __train_iter__(self, X, y, model_state, optimizer_state):
        pass

    @abstractmethod
    def __evaluate_iter__(self, X, y, model_state, metric_states):
        pass

    @staticmethod
    def configure_argument_parser(parser):
        parser.add_argument("--epochs", default=50, type=int, help="Number of epochs")
        parser.add_argument("--logs_dir", default="logs/", type=str, help="Profiler log dirs")
        parser.add_argument("--loss_file", default="loss.json", type=str, help="Loss files")
        parser.add_argument("--metrics_file", default="metrics.json", type=str, help="Metrics file")
        parser.add_argument("--profile", default=False, type=bool, help="Enable profiling")
        parser.add_argument("--initial_model_path", default=None, type=str, help="The path to load trainer state from")
        parser.add_argument("--output_model_path", default=None, type=str,
                            help="The path to write model after training")
        parser.add_argument("--reset_optimizer_state",
                            default=False,
                            type=bool,
                            help="If true will reset optimizer state and learning scheduler")
        parser.add_argument("--checkpoint",
                            default=1000000,
                            type=int,
                            help="If output model path is provided will save model every checkpoint epoch")

    def train(self, train, validation, args):
        state = None

        epochs = args.epochs

        create_new_state = args.initial_model_path is None

        if create_new_state:
            state = self.__new_state__()
        else:
            state = self.__load_state__(args.initial_model_path)

        if args.reset_optimizer_state or create_new_state:
            new_scheduler_state = self.optimizer.scheduler.new_state(len(train) * epochs)
            state = state.with_new_optimizer_state(state.optimizer_state.with_new_scheduler_state(new_scheduler_state))

        self.__print_number_of_parameters__(state)

        for epoch in range(args.epochs):
            _, state = self.__train__(train,
                                      state,
                                      epoch=epoch,
                                      profile=args.profile,
                                      profile_logs_dir=args.logs_dir,
                                      loss_file=args.loss_file)

            self.evaluate(validation, state, args, epoch=epoch)

            if epoch % args.checkpoint == 0 and args.output_model_path is not None:
                jax.debug.print("Saving model")
                self.__save_state__(state, path=args.output_model_path)

        if args.output_model_path is not None:
            self.__save_state__(state, path=args.output_model_path)

        return state

    def evaluate(self, dataset, state, args, **kwargs):
        """
        Evaluate model on dataset and compute metrics
        """
        model_state = state.model_state
        metric_states = self.__new_metric_states__()

        iterator = self.__make_iterator__(dataset, 'Evaluate')

        for X, y in iterator:
            # Transfer tensorflow objects to jax objects
            X, y = from_tensorflow_to_jax(X), from_tensorflow_to_jax(y)
            X = self.converter.convert(X)

            metric_states = self.__evaluate_iter__(X, y, model_state, metric_states)

        jax.block_until_ready(metric_states)

        result = OrderedDict(
            zip(
                [metric.name for metric in self.metrics],
                [state.evaluate() for state in metric_states]
            ),
            **kwargs
        )

        jax.block_until_ready(result)

        for key, value in result.items():
            jax.debug.print('{}: {}', key, value)

        self.__write__(result, args.logs_dir, args.metrics_file)

        return result

    def __train__(self, dataset, state, epoch, profile, profile_logs_dir, loss_file):
        if profile:
            jax.profiler.start_trace(profile_logs_dir)

        losses = []

        model_state = state.model_state
        new_optimizer_state = state.optimizer_state

        iterator = self.__make_iterator__(dataset, f'Train epoch: {epoch}')

        for X, y in iterator:
            # Transfer tensorflow objects to jax objects
            X, y = from_tensorflow_to_jax(X), from_tensorflow_to_jax(y)
            X = self.converter.convert(X)

            model_state, new_optimizer_state, loss_value = self.__train_iter__(X,
                                                                               y,
                                                                               model_state,
                                                                               new_optimizer_state)
            losses += [loss_value]

            next_lr = self.optimizer.scheduler.current_rate(new_optimizer_state.scheduler_state)

            self.__print_info__(iterator, loss_value, next_lr)

        new_state = self.__make_new_state__(model_state, new_optimizer_state)

        jax.block_until_ready(losses)

        if profile:
            jax.profiler.stop_trace()

        result = dict(epoch=epoch, losses=losses)

        self.__write__(result, profile_logs_dir, loss_file)

        return result, new_state

    @staticmethod
    def __print_number_of_parameters__(state):
        def __get_size__(x):
            if isinstance(x, int):
                return 1

            if isinstance(x, float):
                return 1

            return x.size

        get_sizes = lambda x: jax.tree_util.tree_leaves(jax.tree_util.tree_map(__get_size__, x))

        model_parameters = jnp.sum(jnp.array(get_sizes(state.model_state)))
        optimizer_parameters = jnp.sum(jnp.array(get_sizes(state.optimizer_state)))

        jax.debug.print("Number of parameters: {}", model_parameters)
        jax.debug.print("Number of optimizer parameters: {}", optimizer_parameters)

    @classmethod
    def __write__(cls, data, logs_dir, file, append: bool = True):
        file = os.path.join(logs_dir, file)

        data = cls.__convert_to_base_type__(data)
        text = json.dumps(data)

        if not os.path.exists(logs_dir):
            os.mkdir(logs_dir)

        with open(file, "a+" if append else "w+") as file:
            file.write(text + "\n")

    @staticmethod
    def __convert_to_base_type__(data):
        def __to_base_type__(x):
            if isinstance(x, int):
                return x

            if isinstance(x, float):
                return x

            if isinstance(x, str):
                return x

            return x.tolist()

        return jax.tree_util.tree_map(__to_base_type__, data)

    def __load_state__(self, path):
        tree_def = tree_structure(self.__new_state__())

        return load_tree(path, tree_def)

    @staticmethod
    def __save_state__(state, path):
        return write_tree(state, path)

    @staticmethod
    def __make_iterator__(dataset, desc):
        return tqdm(dataset, total=len(dataset), desc=desc, ascii=" >=")

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
