import jax
from jax import jit
from jax.tree_util import register_pytree_node_class

from typing import NamedTuple
from collections import OrderedDict

from src.ensemble.ensemble import base_predictions_key

from src.nn.layers import LayerState, LayerInput
from src.nn.optimizers import Reduction
from src.nn.optimizers.optimizer import GlobalOptimizerState

from .base_trainer import BaseTrainer


class State(NamedTuple):
    model_state: LayerState
    optimizer_state: GlobalOptimizerState

    def with_new_optimizer_state(self, optimizer_state):
        return State(model_state=self.model_state, optimizer_state=optimizer_state)


@register_pytree_node_class
class EnsembleTrainer(BaseTrainer):

    def __new_state__(self):
        model_state = self.initializer.generate(self.model.new_state())

        return State(model_state, optimizer_state=self.optimizer.new_state(model_state))

    def __make_new_state__(self, *params):
        return State(*params)

    def __new_metric_states__(self):
        return [metric.new_state(self.model.estimators) for metric in self.metrics]

    def __print_info__(self, iterator, loss, new_lr):
        iterator.set_postfix(OrderedDict(next_lr=new_lr))

        iterator.write("=============================")

        for key, value in loss.items():
            iterator.write(f"{key}: {value}")

    @jit
    def __train_iter__(self, X, y, model_state, optimizer_state):
        result = self.model.vforward(X, grad=True, state=model_state)

        loss_result = self.loss.evaluate(result.out, result.additional_info[base_predictions_key], y)

        value = LayerInput(loss_result.grad, additional_info={base_predictions_key: loss_result.base_predictor_grad})
        output = self.model.vbackward(value, model_state, result.grad_state)

        _model_state, new_optimizer_state = self.optimizer.vupdate_parameters(
            model_state, output.grad_state, optimizer_state, Reduction.MEAN
        )

        result = OrderedDict(loss_value=loss_result.loss,
                             joint_loss=loss_result.fused_loss,
                             independent_loss=loss_result.independent_loss,
                             base_predictor_losses=loss_result.base_predictor_losses)

        return _model_state, new_optimizer_state, result

    @jit
    def __evaluate_iter__(self, X, y, model_state, metric_states):
        result = self.model.vforward(X, grad=False, state=model_state)

        for index, metric in enumerate(self.metrics):
            metric_states[index] = metric.forward(result.out,
                                                  result.additional_info[base_predictions_key],
                                                  y,
                                                  metric_states[index])

        return metric_states
