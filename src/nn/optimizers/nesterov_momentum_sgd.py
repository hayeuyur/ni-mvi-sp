
import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from .optimizer import Optimizer, OptimizerState
from src.nn.clip import Clip, NoClip
from src.nn.schedulers import Scheduler, ConstantScheduler


class State(OptimizerState):
    momentum: jnp.ndarray


@register_pytree_node_class
class NesterovMomentumSGD(Optimizer):

    def __init__(self, beta: float = 0.9, clip: Clip = NoClip(), scheduler: Scheduler = ConstantScheduler(1e-2)):
        super(NesterovMomentumSGD, self).__init__(clip, scheduler)

        self.beta = beta

    @jit
    def __new_state__(self, params):
        return State(jnp.zeros(params.shape))

    @jit
    def __update__(self, params, gradient, state, learning_rate):
        delta_gradient = learning_rate * gradient

        new_momentum = self.beta * state.momentum - delta_gradient
        new_params = params + self.beta * state.momentum - delta_gradient

        return new_params, State(new_momentum)
