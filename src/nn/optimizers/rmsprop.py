
import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from .optimizer import Optimizer, OptimizerState
from src.nn.clip import Clip, NoClip
from src.nn.schedulers import Scheduler, ConstantScheduler


class State(OptimizerState):
    residual: jnp.ndarray


@register_pytree_node_class
class RMSProp(Optimizer):

    def __init__(self,
                 momentum: float = 0.9,
                 epsilon: float = 1e-7,
                 clip: Clip = NoClip(),
                 scheduler: Scheduler = ConstantScheduler(1e-2)):
        super(RMSProp, self).__init__(clip, scheduler)

        self.momentum = momentum
        self.epsilon = epsilon

    @jit
    def __new_state__(self, params):
        return State(jnp.zeros(params.shape))

    @jit
    def __update__(self, params, gradient, state, learning_rate):
        new_residual = self.momentum * state.residual + (1 - self.momentum) * gradient * gradient
        new_params = params - learning_rate / (jnp.sqrt(new_residual) + self.epsilon) * gradient

        return new_params, State(new_residual)
