from abc import ABC, abstractmethod
from enum import IntEnum
from typing import NamedTuple, Tuple
from functools import partial

import jax
import jax.numpy as jnp
from jax import jit
from jax.tree_util import tree_map

from src.utils.jit import PytreeObject
from src.nn.layers import LayerState, LayerGradState
from src.nn.clip import Clip
from src.nn.schedulers import Scheduler, SchedulerState

from src.utils.tree import split_tree

from typing import List

OptimizerState = NamedTuple


class SparseGrad(NamedTuple):
    """
    Help wrapper to support sparse gradients update. The structure must contain:
      1. `state_shapes` the shapes of original PyTree
      2. `grad_state` the tree of sparse gradient indices, where each row is stacked
      3. `indices` the indices of parameters in the original matrix
    Note, that the reduction will always be
    """
    state_shapes: LayerState
    grad_state: LayerGradState
    indices: LayerState


class Reduction(IntEnum):
    SUM = 1
    MEAN = 2


class GlobalOptimizerState(NamedTuple):
    states: Tuple[OptimizerState]
    scheduler_state: SchedulerState

    def with_new_scheduler_state(self, scheduler_state: SchedulerState):
        return GlobalOptimizerState(states=self.states, scheduler_state=scheduler_state)


class Optimizer(PytreeObject, ABC):

    def __init__(self, clip: Clip, scheduler: Scheduler):
        self.clip = clip
        self.scheduler = scheduler

    def new_state(self, params, steps: int = -1):
        map_result = tree_map(self.__new_state__, params)
        scheduler_state = self.scheduler.new_state(steps)

        return GlobalOptimizerState(map_result, scheduler_state)

    @jit
    def update_parameters(self, state: LayerState, gradient_state: LayerState, optimizer_state: GlobalOptimizerState):
        """
        Update the parameter. (Note, that state, gradient_state and optimizer state must have the same structure)

        :param state: The tree representing the state to update
        :param gradient_state: The tree representing the gradients to update
        :param optimizer_state: The optimizer state
        :return:
        """
        gradient_state = self.clip.clip(gradient_state)
        learning_rate, new_scheduler_state = self.scheduler.new_rate(optimizer_state.scheduler_state)

        result = tree_map(lambda x, y, z: self.__update__(x, y, z, learning_rate),
                          state,
                          gradient_state,
                          optimizer_state.states)

        new_params, new_states = split_tree(result)

        return new_params, GlobalOptimizerState(new_states, new_scheduler_state)

    @partial(jit, static_argnames=["reduction"])
    def vupdate_parameters(self,
                           state: LayerState,
                           gradient_state: LayerState,
                           optimizer_state: GlobalOptimizerState,
                           reduction: Reduction = Reduction.MEAN):
        """
        Update the parameters based on the results from batched output.
        Note, that the state, gradient_state and optimizer_state must have the same structure.
        The gradient state leaf must have additional batch dimension

        :param state: The state of the model
        :param gradient_state: The state of gradient
        :param optimizer_state: The state of the optimizer
        :param reduction: Tells how to combine results from several batches
        :return:
        """

        reduction = reduction.value

        def reduce(grad):
            return jax.lax.switch(reduction,
                                  [lambda x: jnp.sum(x, axis=0),
                                   lambda x: jnp.mean(x, axis=0)],
                                  grad)

        reduced_gradient_state = jax.tree_util.tree_map(reduce, gradient_state)

        return self.update_parameters(state, reduced_gradient_state, optimizer_state)

    @abstractmethod
    @jit
    def __new_state__(self, params):
        pass

    @abstractmethod
    @jit
    def __update__(self, params, gradient, state, learning_rate):
        pass

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
