from jax import jit
from jax.tree_util import register_pytree_node_class

from .optimizer import Optimizer, OptimizerState
from src.nn.clip import Clip, NoClip
from src.nn.schedulers import Scheduler, ConstantScheduler


class State(OptimizerState):
    pass


@register_pytree_node_class
class SGD(Optimizer):

    def __init__(self, clip: Clip = NoClip(), scheduler: Scheduler = ConstantScheduler(1e-2)):
        super(SGD, self).__init__(clip, scheduler)

    def __new_state__(self, params):
        return State()

    @jit
    def __update__(self, params, gradient, state, learning_rate):
        return params - learning_rate * gradient, state
