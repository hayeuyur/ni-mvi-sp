
from .optimizer import Optimizer, OptimizerState
from src.nn.clip import Clip, NoClip
from src.nn.schedulers import Scheduler, ConstantScheduler

import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class


class State(OptimizerState):
    first_momentum: jnp.ndarray
    second_momentum: jnp.ndarray
    timestep: int


@register_pytree_node_class
class AdaBelief(Optimizer):

    def __init__(self,
                 beta_1: float = 0.9,
                 beta_2: float = 0.999,
                 epsilon: float = 1e-7,
                 clip: Clip = NoClip(),
                 scheduler: Scheduler = ConstantScheduler(1e-3)):
        super(AdaBelief, self).__init__(clip=clip, scheduler=scheduler)

        self.beta_1 = beta_1
        self.beta_2 = beta_2

        self.epsilon = epsilon

    @jit
    def __new_state__(self, params):
        return State(jnp.zeros(params.shape), jnp.zeros(params.shape), timestep=0)

    @jit
    def __update__(self, params, gradient, state, learning_rate):
        new_timestep = state.timestep + 1.

        new_first_momentum = self.beta_1 * state.first_momentum + (1. - self.beta_1) * gradient

        difference = gradient - new_first_momentum

        new_second_momentum = self.beta_2 * state.second_momentum +\
                              (1. - self.beta_2) * difference * difference +\
                              self.epsilon

        unbiased_first_momentum = new_first_momentum / (1. - jnp.power(self.beta_1, new_timestep))
        unbiased_second_momentum = new_second_momentum / (1. - jnp.power(self.beta_2, new_timestep))

        update_params = 1. / (jnp.sqrt(unbiased_second_momentum) + self.epsilon) * unbiased_first_momentum
        new_params = params - learning_rate * update_params

        return new_params, State(new_first_momentum, new_second_momentum, new_timestep)
