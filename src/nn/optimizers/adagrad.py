
import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from .optimizer import Optimizer, OptimizerState
from src.nn.clip import Clip, NoClip
from src.nn.schedulers import Scheduler, ConstantScheduler


class State(OptimizerState):
    residual: jnp.ndarray


@register_pytree_node_class
class AdaGrad(Optimizer):

    def __init__(self, epsilon: float = 1e-7, clip: Clip = NoClip(), scheduler: Scheduler = ConstantScheduler(1e-3)):
        super(AdaGrad, self).__init__(clip, scheduler)

        self.epsilon = epsilon

    @jit
    def __new_state__(self, params):
        return State(jnp.zeros(params.shape))

    @jit
    def __update__(self, params, gradient, state, learning_rate):
        new_residual = state.residual + gradient * gradient
        new_params = params - self.learning_rate / (jnp.sqrt(new_residual) + self.epsilon) * gradient

        return new_params, State(new_residual)
