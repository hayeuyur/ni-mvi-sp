
from .scheduler import Scheduler, SchedulerState
from typing import Tuple

import jax
from jax import jit
from jax.tree_util import register_pytree_node_class


class State(SchedulerState):
    step: int
    total_steps: int


@register_pytree_node_class
class ExponentialScheduler(Scheduler):

    def __init__(self, total_steps: int = None, learning_rate: float = 1e-2, end_learning_rate: float = 1e-2):
        super(ExponentialScheduler, self).__init__()

        self.total_steps = total_steps
        self.learning_rate = learning_rate
        self.end_learning_rate = end_learning_rate

    def new_state(self, steps: int = -1) -> State:
        return State(0, steps if self.total_steps is None else self.total_steps)

    @jit
    def current_rate(self, state: State) -> float:
        step = jax.lax.cond(state.step < state.total_steps,
                            lambda: state.step,
                            lambda: state.total_steps)

        decay_rate = self.end_learning_rate / self.learning_rate
        return self.learning_rate * (decay_rate ** (step / state.total_steps))

    @jit
    def new_rate(self, state: State) -> Tuple[float, State]:
        step = jax.lax.cond(state.step < state.total_steps,
                            lambda: state.step + 1,
                            lambda: 1)

        return self.current_rate(state), State(state.step, state.total_steps)
