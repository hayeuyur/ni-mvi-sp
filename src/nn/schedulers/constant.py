
from .scheduler import Scheduler, SchedulerState
from jax.tree_util import register_pytree_node_class
from typing import Tuple
from jax import jit

class State(SchedulerState):
    pass


@register_pytree_node_class
class ConstantScheduler(Scheduler):

    def __init__(self, learning_rate: float = 1e-2):
        self.learning_rate = learning_rate

    def new_state(self, steps: int = -1) -> State:
        return State()

    @jit
    def current_rate(self, state: State) -> float:
        return self.learning_rate

    @jit
    def new_rate(self, state: State) -> Tuple[float, State]:
        return self.current_rate(state), state
