from abc import ABC, abstractmethod
from typing import NamedTuple, Tuple
from src.utils.jit import PytreeObject

SchedulerState = NamedTuple


class Scheduler(PytreeObject, ABC):

    @abstractmethod
    def new_state(self, steps) -> SchedulerState:
        pass

    @abstractmethod
    def current_rate(self, state: SchedulerState) -> float:
        pass

    @abstractmethod
    def new_rate(self, state: SchedulerState) -> Tuple[float, SchedulerState]:
        pass

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
