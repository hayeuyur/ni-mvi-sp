
from .scheduler import Scheduler, SchedulerState
from typing import Tuple

import jax
import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class


class State(SchedulerState):
    step: int
    total_steps: int


@register_pytree_node_class
class CosineScheduler(Scheduler):

    def __init__(self, total_steps: int = None, learning_rate: float = 1e-2, alpha: float = 1.0):
        """

        :param total_steps: The number of steps to perform learning rate restart. If None, then will use
                            the total number of steps to decay
        :param learning_rate: The initial learning rate of the scheduler
        :param alpha: The
        """
        super(CosineScheduler, self).__init__()

        self.total_steps = total_steps
        self.learning_rate = learning_rate
        self.alpha = alpha

    def new_state(self, steps: int = -1) -> State:
        return State(0, steps if self.total_steps is None else self.total_steps)

    @jit
    def current_rate(self, state: State) -> float:
        step = jax.lax.cond(state.step < state.total_steps,
                            lambda: state.step,
                            lambda: state.total_steps)

        cosine_decay = 0.5 * (1 + jnp.cos(jnp.pi * step / state.total_steps))
        return self.learning_rate * ((1 - self.alpha) * cosine_decay + self.alpha)

    @jit
    def new_rate(self, state: State) -> Tuple[float, State]:
        step = jax.lax.cond(state.step < state.total_steps,
                            lambda: state.step + 1,
                            lambda: 1)

        return self.current_rate(state), State(step, state.total_steps)
