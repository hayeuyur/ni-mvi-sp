from abc import ABC, abstractmethod

import jax.numpy as jnp

from jax import jit
from jax import random
from jax.tree_util import tree_map, register_pytree_node_class

from src.utils.jit import PytreeObject

from typing import Any, Callable, List, Dict, NamedTuple


# TODO: Implement better wrap into initializer
@register_pytree_node_class
class OverrideInfo(PytreeObject):
    def __init__(self, tree, initializer, args, kwargs):
        super(OverrideInfo, self).__init__()

        self.initializer = initializer
        self.tree = tree
        self.args = args
        self.kwargs = kwargs

    def make_initializer(self, key):
        return self.initializer(*self.args, **self.kwargs, key=key)

    def tree_flatten(self):
        children = (self.tree,)
        aux_data = {"initializer": self.initializer, "args": self.args, "kwargs": self.kwargs}

        return children, aux_data


def override_initializer(initializer, tree, *args, **kwargs):
    return OverrideInfo(tree, initializer, args, kwargs)


class Initializer(PytreeObject, ABC):

    def __init__(self, key):
        self.key = key

    def generate(self, parameter):
        def generate_param(_parameter):
            key, subkey = random.split(self.key)

            if isinstance(_parameter, OverrideInfo):
                _initializer = _parameter.make_initializer(subkey)

                return _initializer.generate(_parameter.tree)

            self.key = key

            return self.__generate__(subkey, _parameter.shape, _parameter.dtype)

        return tree_map(generate_param, parameter, is_leaf=lambda x: isinstance(x, OverrideInfo))

    @abstractmethod
    def __generate__(self, key, shape, dtype):
        pass

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
