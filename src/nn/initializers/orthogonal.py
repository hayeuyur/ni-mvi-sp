
import jax
import jax.numpy as jnp
import jax.random
from jax.tree_util import register_pytree_node_class

from src.nn.initializers.initializer import Initializer


@register_pytree_node_class
class Orthogonal(Initializer):
    """
    The implementation of the orthogonal initializer taken from the
    https://jax.readthedocs.io/en/latest/_modules/jax/_src/nn/initializers.html#orthogonal
    """

    def __init__(self, key, scale: float = 1, axis: int = -1):
        super(Orthogonal, self).__init__(key)

        self.scale = scale
        self.axis = axis

    def __generate__(self, key, shape, dtype):
        shape = jnp.array(shape)
        rows, cols = jnp.prod(shape) // shape[self.axis], shape[self.axis]
        matrix_shape = (cols, rows) if rows < cols else rows, cols

        matrix = jax.random.normal(key, matrix_shape, dtype=dtype)
        Q, R = jnp.linalg.qr(matrix)

        diag_sign = jax.lax.broadcast_to_rank(jnp.sign(jnp.diag(R)), rank=Q.ndim)
        Q *= diag_sign  # needed for a uniform distribution

        if rows < cols:
            Q = Q.T

        Q = jnp.reshape(Q, tuple(jnp.delete(shape, self.axis)) + (shape[self.axis],))
        Q = jnp.moveaxis(Q, -1, self.axis)

        return self.scale * Q
