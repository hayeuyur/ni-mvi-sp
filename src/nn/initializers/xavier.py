
from jax import numpy as jnp
from jax import random
from jax.tree_util import register_pytree_node_class

from src.nn.initializers.initializer import Initializer


@register_pytree_node_class
class Xavier(Initializer):
    """
    The implementation of weight initialization from  "Understanding the difficulty of training deep
    feedforward neural networks" by Xavier Glorot, Yoshua Bengio.
    """
    def __init__(self, key, scale: float = 1):
        super(Xavier, self).__init__(key=key)

        self.scale = scale

    def __generate__(self, key, shape, dtype):
        value = jnp.sqrt(6 / jnp.sum(jnp.array(shape)))

        return self.scale * random.uniform(minval=-value, maxval=value, shape=shape, key=key, dtype=dtype)
