
import jax.random
from jax.tree_util import register_pytree_node_class

from src.nn.initializers.initializer import Initializer


@register_pytree_node_class
class Normal(Initializer):

    def __init__(self, key, scale: float = 1e-02):
        super(Normal, self).__init__(key=key)

        self.scale = scale

    def __generate__(self, key, shape, dtype):
        return self.scale * jax.random.normal(shape=shape, key=key, dtype=dtype)

