
import jax.numpy as jnp
from jax.tree_util import register_pytree_node_class

from src.nn.initializers.initializer import Initializer


@register_pytree_node_class
class Constant(Initializer):

    def __init__(self, key, value: float = 1):
        super(Constant, self).__init__(key)

        self.value = value

    def __generate__(self, key, shape, dtype):
        return self.value * jnp.ones(shape, dtype=dtype)
