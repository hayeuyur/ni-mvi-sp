from .loss import Loss
from src.utils.utils import softmax, mean_sparse_crossentropy_loss, sparse_crossentropy_loss_gradient

import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class


@register_pytree_node_class
class CrossEntropyLoss(Loss):

    def __init__(self, is_probability_distribution: bool = False, axis=-1, sum_axis=-1):
        """
        :param axis: The axis of probability distribution
        :param sum_axis: The axis to perform sum over elements. For example, in the RNN we want to minimize
                         sum of the sparse cross entropy loss across sequence. Therefore, we want to perform
                         double sum over the sequence and over distribution
        """

        self.is_probability_distribution = is_probability_distribution
        self.axis = axis
        self.sum_axis = sum_axis

    @jit
    def evaluate(self, predictions, targets, grad: bool = True):
        if not self.is_probability_distribution:
            predictions = softmax(predictions, axis=self.axis)

        loss = mean_sparse_crossentropy_loss(predictions, targets, axis=self.sum_axis)

        gradient = jnp.where(grad,
                             sparse_crossentropy_loss_gradient(predictions, targets),
                             jnp.zeros(predictions.shape))

        return loss, gradient
