from abc import ABC, abstractmethod
from jax import jit

from src.utils.jit import PytreeObject


class Loss(PytreeObject, ABC):

    @abstractmethod
    @jit
    def evaluate(self, predictions, targets, grad: bool = True):
        pass

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
