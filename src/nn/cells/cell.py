from abc import ABC, abstractmethod

import jax.lax
import jax.numpy as jnp
from jax import jit

from src.nn.layers.layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput
from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState
from src.nn.initializers import Orthogonal, Constant, override_initializer

from typing import Tuple, List, NamedTuple
from functools import partial

HiddenState = NamedTuple

activation_key = "activation"
cell_state_key = "cell_state"


class Weights(NamedTuple):
    input_weights: jnp.ndarray
    activation_weights: jnp.ndarray
    bias: jnp.ndarray


class Cell(Layer, ABC):

    def __init__(self, unroll: int = 1):
        """
        :param unroll: The number of iterations to unroll
        """
        super(Cell, self).__init__()
        self.unroll = unroll

    @abstractmethod
    @jit
    def __new_hidden_state__(self, inputs: LayerInput) -> HiddenState:
        """
        Creates CellState from LayerInput

        :param inputs: The inputs received on the forward
        :return:
        """
        pass

    @abstractmethod
    @jit
    def __new_grad_state__(self, sequence, *cache) -> LayerGradState:
        pass

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: LayerState) -> LayerOutput:
        """
        Forward pass of the RNN cell

        :param inputs: Either the tuple of the sequence and activation or sequence only.
                       The shape of the sequence is (seq_length, embedding_dim) and the shape of the activation is
                       (hidden_shape_dim). If no initial activation is provided, then it will be zero
        :param grad: true, if the network must perform backward pass
        :param state: state of the cell
        :return:
        """
        sequence = inputs.input
        hidden_state = self.__new_hidden_state__(inputs)

        @jit
        def scanned(inner_hidden_state, token):
            new_hidden_state, activation_cache = self.__inner_forward__(token, inner_hidden_state, state)

            return new_hidden_state, (inner_hidden_state, activation_cache)

        output_hidden_state, cache = jax.lax.scan(scanned, init=hidden_state, xs=sequence, unroll=self.unroll)

        # Compute outputs. First activation in cache is the input activation
        outputs = jnp.vstack([cache[0].activation[1:], output_hidden_state.activation])

        return LayerOutput(outputs, self.__new_grad_state__(sequence, cache), output_hidden_state._asdict())

    @jit
    def __backward__(self, gradient: LayerInput, state: LayerState, grad_state: LayerGradState) -> LayerOutput:
        """
        Backward pass of the RNN cell

        :param gradient: Layer input, there the inputs are gradients on the output channel and hidden_state
                         gradient is in the `additional_info`
        :param state: State to compute
        :param grad_state: Gradient state to compute
        :return:
        """
        sequence_gradients = gradient.input
        hidden_state_gradient = self.__new_hidden_state__(gradient)

        @jit
        def scanned(accumulated, pair) -> Tuple[Tuple[HiddenState, LayerState], List[jnp.ndarray]]:
            hidden_state_grad, gradients = accumulated
            prediction_gradient, _grad_state = pair

            backward_result = self.__inner_backward__(prediction_gradient, hidden_state_grad, state, _grad_state)

            new_hidden_state_grad, param_grads, input_gradient = backward_result

            @jit
            def _sum(x, y):
                return x + y

            # Accumulate gradients due to multivariate chain rule
            gradients = jax.tree_util.tree_map(_sum, gradients, param_grads)

            return (new_hidden_state_grad, gradients), input_gradient

        gradients_state = jax.tree_util.tree_map(lambda x: jnp.zeros_like(x), state)

        # Inputs gradients are already in the order of the sequence gradients
        result, inputs_gradient = jax.lax.scan(scanned,
                                               init=(hidden_state_gradient, gradients_state),
                                               xs=(sequence_gradients, grad_state),
                                               reverse=True,
                                               unroll=self.unroll)

        _hidden_state_grad, grads = result

        return LayerOutput(jnp.vstack(inputs_gradient), grads, _hidden_state_grad._asdict())

    @abstractmethod
    @jit
    def __inner_forward__(self, token, hidden_state: LayerState, state: LayerState):
        """
        :param token: Token to push forward
        :param hidden_state: The hidden state of the cell
        :param state: The state of the layer
        :return:
        """
        pass

    @abstractmethod
    @jit
    def __inner_backward__(self, gradient, hidden_state_grad: HiddenState, state: LayerState, grad_state: LayerGradState):
        """
        :param gradient: The gradient on prediction channel
        :param hidden_state_grad: The gradient on next cell state channel
        :param state: The state of the layer
        :param grad_state: The gradient state created during forward call
        :return:
        """
        pass

    @staticmethod
    @jit
    def __backward_parameters__(gradient,
                                inputs,
                                hidden_state,
                                weights: Weights,
                                activation: UnaryOperator,
                                activation_gradient: OperatorGradState):
        bias_grad = activation.backward(gradient, activation_gradient)

        # Check linear.py to get intuition, why it must be performed this way
        input_weights_grad = inputs[:, jnp.newaxis] * bias_grad[jnp.newaxis, :]
        activation_weights_grad = hidden_state[:, jnp.newaxis] * bias_grad[jnp.newaxis, :]

        output_activation_gradient = bias_grad @ weights.activation_weights.T
        inputs_gradient = bias_grad @ weights.input_weights.T

        grads = Weights(input_weights_grad, activation_weights_grad, bias_grad)

        return output_activation_gradient, grads, inputs_gradient

    @staticmethod
    def make_default_weights(input_shape, output_shape, bias_value: int = 0):
        return Weights(
            input_weights=jnp.zeros((input_shape, output_shape)),
            activation_weights=override_initializer(Orthogonal, jnp.zeros((output_shape, output_shape))),
            bias=override_initializer(Constant, jnp.zeros((output_shape,)), value=bias_value)
        )

    @staticmethod
    @jit
    def dot(token, activation, weights: Weights):
        return jnp.dot(token, weights.input_weights) + \
               jnp.dot(activation, weights.activation_weights) + \
               weights.bias

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
