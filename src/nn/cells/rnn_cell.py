import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from src.nn.layers.layer import LayerState, LayerGradState, LayerInput

from .cell import Cell, Weights, activation_key
from .cell import HiddenState as _HiddenState


from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState


class State(LayerState):
    weights: Weights


class HiddenState(_HiddenState):
    activation: jnp.ndarray


HiddenGradState = HiddenState


class GradState(LayerGradState):
    input: jnp.ndarray
    hidden_state: HiddenState
    activation_state: OperatorGradState


@register_pytree_node_class
class RNNCell(Cell):

    def __init__(self,
                 input_shape: jnp.ndarray,
                 output_shape: jnp.ndarray,
                 activation: UnaryOperator,
                 unroll: int = 32):
        """
        :param input_shape: the shape of input
        :param output_shape: the output shape of the cell
        :param activation: activation function
        :param unroll: number of iterations to unroll
        """
        super(RNNCell, self).__init__()

        self.input_shape = input_shape
        self.output_shape = output_shape
        self.activation = activation
        self.unroll = unroll

    def new_state(self):
        weights = self.make_default_weights(self.input_shape, self.output_shape)

        return State(weights)

    @jit
    def __new_hidden_state__(self, inputs: LayerInput) -> HiddenState:
        activation = inputs.additional_info.get(activation_key)

        if activation is None:
            activation = jnp.zeros(self.output_shape)

        return HiddenState(activation)

    @jit
    def __new_grad_state__(self, sequence, cache) -> LayerGradState:
        return GradState(sequence, *cache)

    @jit
    def __inner_forward__(self, token, hidden_state: HiddenState, state: State):
        hidden = self.dot(token, hidden_state.activation, state.weights)
        output = self.activation.forward(hidden)

        return HiddenState(output.out), output.grad_state

    @jit
    def __inner_backward__(self, gradient, hidden_state_grad: HiddenGradState, state: State, grad_state: GradState):
        activation_gradient, grad_state, inputs_gradient = self.__backward_parameters__(
            gradient + hidden_state_grad.activation,
            grad_state.input,
            grad_state.hidden_state.activation,
            state.weights,
            self.activation,
            grad_state.activation_state
        )

        new_hidden_state = HiddenState(activation_gradient)
        grads = State(grad_state)

        return new_hidden_state, grads, inputs_gradient



