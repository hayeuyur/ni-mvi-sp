
import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from src.nn.layers.layer import LayerState, LayerGradState, LayerInput
from .cell import Cell, Weights, activation_key
from .cell import HiddenState as _HiddenState

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState
from src.nn.operators.binary import Multiply

from typing import NamedTuple


class State(LayerState):
    reset_gate_weights: Weights
    update_gate_weights: Weights
    output_gate_weights: Weights


class HiddenState(_HiddenState):
    activation: jnp.ndarray


HiddenGradState = HiddenState


class GradState(LayerGradState):
    class ActivationsGradState(NamedTuple):
        update_gate_activation: OperatorGradState
        output_gate_activation: OperatorGradState
        reset_gate_activation: OperatorGradState

    class MultiplicationGradState(NamedTuple):
        reset_gate_multiply_grad_state: OperatorGradState
        update_gate_multiply_grad_state: OperatorGradState
        output_multiply_grad_state: OperatorGradState

    input: jnp.ndarray
    hidden_state: HiddenState
    activations: ActivationsGradState
    multiplications: MultiplicationGradState


@register_pytree_node_class
class GRUCell(Cell):

    def __init__(self,
                 input_shape: jnp.ndarray,
                 output_shape: jnp.ndarray,
                 activation: UnaryOperator,
                 recurrent_activation: UnaryOperator,
                 multiply: Multiply = Multiply(),
                 unroll: int = 32):
        """
        :param input_shape: the shape of input
        :param output_shape: the output shape of the cell
        :param activation: activation function
        :param unroll: number of iterations to unroll
        """
        super(GRUCell, self).__init__()

        self.input_shape = input_shape
        self.output_shape = output_shape
        self.activation = activation
        self.recurrent_activation = recurrent_activation
        self.multiply = multiply
        self.unroll = unroll

    def new_state(self):
        weights = self.make_default_weights(self.input_shape, self.output_shape)

        return State(reset_gate_weights=weights,
                     update_gate_weights=weights,
                     output_gate_weights=weights)

    @jit
    def __new_hidden_state__(self, inputs: LayerInput) -> HiddenState:
        activation = inputs.additional_info.get(activation_key) or jnp.zeros(self.output_shape)

        return HiddenState(activation)

    @jit
    def __new_grad_state__(self, sequence, cache) -> LayerGradState:
        return GradState(sequence, cache[0], *cache[1])

    @jit
    def __inner_forward__(self, token, hidden_state: HiddenState, state: State):
        reset_gate_hidden = self.dot(token, hidden_state.activation, state.reset_gate_weights)
        reset_gate_out = self.activation.forward(reset_gate_hidden)

        update_gate_hidden = self.dot(token, hidden_state.activation, state.update_gate_weights)
        update_gate_out = self.activation.forward(update_gate_hidden)

        reset_gate_multiplication = self.multiply.forward(reset_gate_out.out, hidden_state.activation)

        output_gate_hidden = self.dot(token, reset_gate_multiplication.out, state.output_gate_weights)
        output_gate_out = self.recurrent_activation.forward(output_gate_hidden)

        update_gate_multiplication = self.multiply.forward(1. - update_gate_out.out, hidden_state.activation)
        output_gate_multiplication = self.multiply.forward(update_gate_out.out, output_gate_out.out)

        new_hidden_state = HiddenState(update_gate_multiplication.out + output_gate_multiplication.out)

        activation_grad_state = GradState.ActivationsGradState(
            update_gate_out.grad_state,
            output_gate_out.grad_state,
            reset_gate_out.grad_state
        )

        multiplication_grad_state = GradState.MultiplicationGradState(
            reset_gate_multiplication.grad_state,
            update_gate_multiplication.grad_state,
            output_gate_multiplication.grad_state
        )

        return new_hidden_state, (activation_grad_state, multiplication_grad_state)

    @jit
    def __inner_backward__(self, gradient, hidden_state_grad: HiddenGradState, state: State, grad_state: GradState):
        @jit
        def __backward__(grad, weights, activation, _activation_grad):
            return self.__backward_parameters__(
                grad, grad_state.input, grad_state.hidden_state.activation, weights, activation, _activation_grad
            )

        activation_grad = gradient + hidden_state_grad.activation

        first_update_gate_grad, output_gate_grad = self.multiply.backward(
            activation_grad, grad_state.multiplications.output_multiply_grad_state
        )

        # Output gate
        output_gate_activation_gradient, output_grad_state, output_inputs_gradient = __backward__(
            output_gate_grad,
            state.output_gate_weights,
            self.recurrent_activation,
            grad_state.activations.output_gate_activation
        )

        # Update gate
        second_update_grad, first_activation_grad = self.multiply.backward(
            activation_grad, grad_state.multiplications.update_gate_multiply_grad_state
        )

        update_gate_grad = first_update_gate_grad - second_update_grad

        update_gate_activation_gradient, update_grad_state, update_inputs_gradient = __backward__(
            update_gate_grad,
            state.update_gate_weights,
            self.activation,
            grad_state.activations.update_gate_activation
        )

        # Reset gate
        reset_gate_grad, second_activation_grad = self.multiply.backward(
            output_gate_activation_gradient, grad_state.multiplications.reset_gate_multiply_grad_state
        )

        reset_gate_activation_gradient, reset_grad_state, reset_inputs_gradient = __backward__(
            reset_gate_grad,
            state.reset_gate_weights,
            self.activation,
            grad_state.activations.reset_gate_activation
        )

        out_activation_grad = first_activation_grad + \
                              second_activation_grad + \
                              update_gate_activation_gradient + \
                              reset_gate_activation_gradient

        inputs_grad = output_inputs_gradient + reset_inputs_gradient + update_inputs_gradient

        new_hidden_state = HiddenState(out_activation_grad)
        grads = State(reset_grad_state, update_grad_state, output_grad_state)

        return new_hidden_state, grads, inputs_grad
