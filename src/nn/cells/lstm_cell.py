import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from src.nn.layers.layer import LayerState, LayerGradState, LayerInput
from .cell import Cell, Weights, activation_key, cell_state_key
from .cell import HiddenState as _HiddenState

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState
from src.nn.operators.binary import Multiply

from typing import NamedTuple


class State(LayerState):
    forget_gate_weights: Weights
    left_candidate_weights: Weights
    right_candidate_weights: Weights
    output_gate_weights: Weights


class HiddenState(_HiddenState):
    activation: jnp.ndarray
    cell_state: jnp.ndarray


HiddenGradState = HiddenState


class GradState(LayerGradState):
    class ActivationsGradState(NamedTuple):
        forget_gate_activation: OperatorGradState
        left_candidate_activation: OperatorGradState
        right_candidate_activation: OperatorGradState
        output_gate_activation: OperatorGradState
        recurrent_activation: OperatorGradState

    class MultiplicationGradState(NamedTuple):
        forget_gate_multiply_grad_state: OperatorGradState
        candidate_multiply_grad_state: OperatorGradState
        output_multiply_grad_state: OperatorGradState

    input: jnp.ndarray
    hidden_state: HiddenState
    activations: ActivationsGradState
    multiplications: MultiplicationGradState


@register_pytree_node_class
class LSTMCell(Cell):

    def __init__(self,
                 input_shape: jnp.ndarray,
                 output_shape: jnp.ndarray,
                 activation: UnaryOperator,
                 recurrent_activation: UnaryOperator,
                 multiply: Multiply = Multiply(),
                 unroll: int = 32):
        """
        :param input_shape: the shape of input
        :param output_shape: the output shape of the cell
        :param activation: activation function
        :param unroll: number of iterations to unroll
        """
        super(LSTMCell, self).__init__()

        self.input_shape = input_shape
        self.output_shape = output_shape
        self.activation = activation
        self.recurrent_activation = recurrent_activation
        self.multiply = multiply
        self.unroll = unroll

    def new_state(self):
        forget_gate_weights = self.make_default_weights(self.input_shape, self.output_shape, bias_value=1)
        weights = self.make_default_weights(self.input_shape, self.output_shape)

        return State(forget_gate_weights=forget_gate_weights,
                     left_candidate_weights=weights,
                     right_candidate_weights=weights,
                     output_gate_weights=weights)

    @jit
    def __new_hidden_state__(self, inputs: LayerInput) -> HiddenState:
        cell_state = inputs.additional_info.get(cell_state_key) or jnp.zeros(self.output_shape)
        activation = inputs.additional_info.get(activation_key) or jnp.zeros(self.output_shape)

        return HiddenState(activation, cell_state)

    @jit
    def __new_grad_state__(self, sequence, cache) -> LayerGradState:
        return GradState(sequence, cache[0], *cache[1])

    @jit
    def __inner_forward__(self, token, hidden_state: HiddenState, state: State):
        # Compute hidden values
        forget_gate_hidden = self.dot(token, hidden_state.activation, state.forget_gate_weights)
        left_candidate_hidden = self.dot(token, hidden_state.activation, state.left_candidate_weights)
        right_candidate_hidden = self.dot(token, hidden_state.activation, state.right_candidate_weights)
        output_hidden = self.dot(token, hidden_state.activation, state.output_gate_weights)

        # Push through activation functions
        forget_gate_out = self.activation.forward(forget_gate_hidden)
        left_candidate_out = self.activation.forward(left_candidate_hidden)
        right_candidate_out = self.recurrent_activation.forward(right_candidate_hidden)
        output_out = self.activation.forward(output_hidden)

        # Compute new hidden state
        cell_state = hidden_state.cell_state

        state_forget_multiply_out = self.multiply.forward(cell_state, forget_gate_out.out)
        candidates_multiply_out = self.multiply.forward(left_candidate_out.out, right_candidate_out.out)

        new_cell_state = state_forget_multiply_out.out + candidates_multiply_out.out

        # Compute new activation
        recurrent_activation_out = self.recurrent_activation.forward(new_cell_state)
        new_activation_out = self.multiply.forward(output_out.out, recurrent_activation_out.out)

        new_hidden_state = HiddenState(new_activation_out.out, new_cell_state)

        activation_grad_state = GradState.ActivationsGradState(
            forget_gate_out.grad_state,
            left_candidate_out.grad_state,
            right_candidate_out.grad_state,
            output_out.grad_state,
            recurrent_activation_out.grad_state
        )

        multiplication_grad_state = GradState.MultiplicationGradState(
            state_forget_multiply_out.grad_state,
            candidates_multiply_out.grad_state,
            new_activation_out.grad_state
        )

        return new_hidden_state, (activation_grad_state, multiplication_grad_state)

    @jit
    def __inner_backward__(self, gradient, hidden_state_grad: HiddenGradState, state: State, grad_state: GradState):
        activation_grad = gradient + hidden_state_grad.activation

        output_gate_grad, cell_state_grad = self.multiply.backward(
            activation_grad, grad_state.multiplications.output_multiply_grad_state
        )

        cell_state_grad = self.recurrent_activation.backward(
            cell_state_grad, grad_state.activations.recurrent_activation
        )

        cell_state_grad = cell_state_grad + hidden_state_grad.cell_state

        left_candidate_grad, right_candidate_grad = self.multiply.backward(
            cell_state_grad, grad_state.multiplications.candidate_multiply_grad_state
        )

        cell_state_grad, forget_gate_grad = self.multiply.backward(
            cell_state_grad, grad_state.multiplications.forget_gate_multiply_grad_state
        )

        @jit
        def __backward__(grad, weights, activation, _activation_grad):
            return self.__backward_parameters__(
                grad, grad_state.input, grad_state.hidden_state.activation, weights, activation, _activation_grad
            )

        output_gate_activation_gradient, output_grad_state, output_inputs_gradient = __backward__(
            output_gate_grad, state.output_gate_weights, self.activation, grad_state.activations.output_gate_activation
        )

        forget_gate_activation_gradient, forget_gate_grad_state, forget_gate_inputs_gradient = __backward__(
            forget_gate_grad, state.forget_gate_weights, self.activation, grad_state.activations.forget_gate_activation
        )

        left_candidate_activation_gradient, left_candidate_grad_state, left_candidate_inputs_gradient = __backward__(
            left_candidate_grad,
            state.left_candidate_weights,
            self.activation,
            grad_state.activations.left_candidate_activation
        )

        right_candidate_activation_gradient, right_candidate_grad_state, right_candidate_inputs_gradient = __backward__(
            right_candidate_grad,
            state.right_candidate_weights,
            self.recurrent_activation,
            grad_state.activations.right_candidate_activation
        )

        # The result of the multivariate chain rule
        out_activation_grad = output_gate_activation_gradient + forget_gate_activation_gradient + \
                              left_candidate_activation_gradient + right_candidate_activation_gradient

        inputs_grad = output_inputs_gradient + forget_gate_inputs_gradient + \
                      left_candidate_inputs_gradient + right_candidate_inputs_gradient

        new_hidden_state = HiddenState(out_activation_grad, cell_state_grad)
        grads = State(forget_gate_grad_state, left_candidate_grad_state, right_candidate_grad_state, output_grad_state)

        return new_hidden_state, grads, inputs_grad
