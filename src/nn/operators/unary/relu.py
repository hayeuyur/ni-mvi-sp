
from src.nn.layers.layer import LayerOutput

import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState


class GradState(OperatorGradState):
    inputs: jnp.ndarray


@register_pytree_node_class
class ReLU(UnaryOperator):

    @partial(jit, static_argnames="grad")
    def __activation_forward__(self, inputs: jnp.ndarray, grad: bool) -> LayerOutput:
        a = jnp.maximum(jnp.asarray(0.), inputs)

        return LayerOutput(jnp.maximum(jnp.asarray(0.), inputs),
                           GradState(inputs),
                           additional_info=dict())

    @jit
    def __activation_backward__(self, grad_state: GradState) -> jnp.ndarray:
        return grad_state.inputs > 0
