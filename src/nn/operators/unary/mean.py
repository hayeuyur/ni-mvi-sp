
from src.nn.layers.layer import LayerOutput

import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial
from typing import Tuple

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState


class GradState(OperatorGradState):
    template: jnp.ndarray


@register_pytree_node_class
class Mean(UnaryOperator):
    """
    A simple layer to perform mean of input values
    """
    def __init__(self, scale_grad: bool = True, axis=0):
        """
        :param axis: Axis to calculate mean
        """
        super(Mean, self).__init__()

        self.scale_grad = scale_grad
        self.axis = axis

    @partial(jit, static_argnames="grad")
    def __activation_forward__(self, inputs: jnp.ndarray, grad: bool) -> LayerOutput:
        result = jnp.mean(inputs, axis=self.axis)

        # Create the template matrix of ones of shape (..., self.axis, 1, ...)
        # To correctly rescale gradients in backward pass
        new_shape = list(inputs.shape)
        new_shape[self.axis + 1] = 1

        template = jnp.ones(new_shape)

        return LayerOutput(result, GradState(template), additional_info=dict())

    @jit
    def __activation_backward__(self, grad_state: GradState) -> jnp.ndarray:
        # Set the shape to (..., self.axis, 1, ...) to correctly perform broadcasting
        scale = jnp.where(self.scale_grad, 1. / grad_state.template[self.axis], 1.)

        return scale * grad_state.template
