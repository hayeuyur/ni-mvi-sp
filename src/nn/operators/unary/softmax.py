
from src.nn.layers.layer import LayerOutput

import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState
from src.utils.utils import softmax


class GradState(OperatorGradState):
    pass


# TODO: Implement correct gradient
@register_pytree_node_class
class Softmax(UnaryOperator):
    """
    A simple layer to perform softmax and no backward gradient

    WARNING: Expect Sparse Cross Entropy Loss to work correctly
    """
    def __init__(self, axis=-1):
        """
        :param axis: Axis to calculate softmax
        """
        super(Softmax, self).__init__()
        self.axis = axis

    @partial(jit, static_argnames="grad")
    def __activation_forward__(self, inputs: jnp.ndarray, grad: bool) -> LayerOutput:
        result = softmax(inputs, axis=self.axis)

        return LayerOutput(result, GradState(), additional_info=dict())

    @jit
    def __activation_backward__(self, grad_state: GradState) -> jnp.ndarray:
        return 1.
