
from src.nn.layers.layer import LayerOutput

import jax
import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState


class GradState(OperatorGradState):
    inputs: jnp.ndarray
    hidden: jnp.ndarray


@register_pytree_node_class
class GeLU(UnaryOperator):

    def __init__(self, approximate: bool = False):
        super(GeLU, self).__init__()

        self.approximate = approximate

    @partial(jit, static_argnames="grad")
    def __activation_forward__(self, inputs: jnp.ndarray, grad: bool) -> LayerOutput:
        def approximate_gelu(x):
            hidden = jnp.tanh(jnp.sqrt(2 / jnp.pi) * (x + 0.044715 * x ** 3))
            result = 0.5 * x * (1 + hidden)

            return result, hidden

        def gelu(x):
            hidden = jax.lax.erf(x / jnp.sqrt(2))
            result = 0.5 * x * (1 + hidden)

            return result, hidden

        output, hidden = jax.lax.cond(
            self.approximate,
            lambda x: approximate_gelu(x),
            lambda x: gelu(x),
            inputs
        )

        return LayerOutput(output,
                           GradState(inputs, hidden),
                           additional_info=dict())

    @jit
    def __activation_backward__(self, grad_state: GradState) -> jnp.ndarray:
        s = grad_state.inputs / jnp.sqrt(2)
        prime = (2. / jnp.sqrt(jnp.pi)) * jnp.exp(-(s ** 2.))

        return 0.5 + 0.5 * grad_state.hidden + ((0.5 * grad_state.inputs * prime) / jnp.sqrt(2.))
