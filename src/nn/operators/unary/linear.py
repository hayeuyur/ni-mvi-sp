
from src.nn.layers.layer import LayerOutput

import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState


class GradState(OperatorGradState):
    pass


@register_pytree_node_class
class Linear(UnaryOperator):

    @partial(jit, static_argnames="grad")
    def __activation_forward__(self, inputs: jnp.ndarray, grad: bool) -> LayerOutput:
        return LayerOutput(inputs, GradState(), additional_info=dict())

    @jit
    def __activation_backward__(self, grad_state: GradState) -> jnp.ndarray:
        return jnp.asarray(1)
