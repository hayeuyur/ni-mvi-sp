
from src.nn.layers.layer import LayerOutput

import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState


class GradState(OperatorGradState):
    inputs: jnp.ndarray
    sigmoid: jnp.ndarray


@register_pytree_node_class
class Swish(UnaryOperator):

    def __init__(self, beta: float = 1.0):
        super(Swish, self).__init__()

        self.beta = beta

    @partial(jit, static_argnames="grad")
    def __activation_forward__(self, inputs: jnp.ndarray, grad: bool) -> LayerOutput:
        sigmoid = (1. / (1. + jnp.exp(-self.beta * inputs)))

        return LayerOutput(inputs * sigmoid,
                           GradState(inputs, sigmoid),
                           additional_info=dict())

    @jit
    def __activation_backward__(self, grad_state: GradState) -> jnp.ndarray:
        scaled_inputs = self.beta * grad_state.inputs

        return grad_state.sigmoid * (1 + scaled_inputs - scaled_inputs * grad_state.sigmoid)
