
from abc import ABC, abstractmethod

from jax import jit, vmap
from jax.tree_util import tree_map
import jax.numpy as jnp

from functools import partial

from typing import NamedTuple, Union, Sequence, Any, Tuple

from src.utils.jit import PytreeObject
from src.nn.layers.layer import LayerOutput


OperatorGradState = NamedTuple


class BinaryOperator(PytreeObject, ABC):

    @partial(jit, static_argnames="grad")
    def forward(self, lhs: PytreeObject, rhs: PytreeObject, grad: bool = True) -> LayerOutput:
        return self.__forward__(lhs, rhs, grad)

    @partial(jit, static_argnames=("grad", "in_axes", "out_axes"))
    def vforward(self, lhs: PytreeObject, rhs: PytreeObject, grad: bool = True, in_axes=0, out_axes=0) -> LayerOutput:
        return vmap(lambda x, y: self.__forward__(x, y, grad), in_axes=in_axes, out_axes=out_axes)(lhs, rhs)

    @jit
    def backward(self, gradient: PytreeObject, grad_state: OperatorGradState) -> PytreeObject:
        return self.__backward__(gradient, grad_state)

    @partial(jit, static_argnames=["in_axes", "out_axes"])
    def vbackward(self,
                  gradient: PytreeObject,
                  grad_state: PytreeObject,
                  in_axes: Union[int, Sequence[Any]] = (0, 0),
                  out_axes: Union[int, Sequence[Any]] = 0) -> LayerOutput:
        return vmap(
            lambda _gradient, _grad_state: self.__backward__(_gradient, _grad_state),
            in_axes=in_axes,
            out_axes=out_axes
        )(gradient, grad_state)

    @partial(jit, static_argnames="grad")
    def __forward__(self, lhs: PytreeObject, rhs: PytreeObject, grad: bool) -> LayerOutput:
        return tree_map(lambda x, y: self.__activation_forward__(x, y, grad), lhs, rhs)

    @jit
    def __backward__(self, gradient: PytreeObject, grad_state: OperatorGradState) -> PytreeObject:
        return tree_map(lambda _gradient, _grad_state: self.__activation_backward__(_gradient, _grad_state),
                        gradient,
                        grad_state)

    @abstractmethod
    @partial(jit, static_argnames="grad")
    def __activation_forward__(self, lhs: jnp.ndarray, rhs: jnp.ndarray, grad: bool) -> LayerOutput:
        pass

    @abstractmethod
    @jit
    def __activation_backward__(self,
                                gradient: jnp.ndarray,
                                grad_state: OperatorGradState) -> Tuple[jnp.ndarray, jnp.ndarray]:
        pass

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
