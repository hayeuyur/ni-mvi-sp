
from src.nn.layers import LayerOutput

import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial
from typing import Tuple

from src.nn.operators.binary_operator import BinaryOperator, OperatorGradState


class GradState(OperatorGradState):
    lhs: jnp.ndarray
    rhs: jnp.ndarray


@register_pytree_node_class
class Multiply(BinaryOperator):

    @partial(jit, static_argnames="grad")
    def __activation_forward__(self, lhs: jnp.ndarray, rhs: jnp.ndarray, grad: bool) -> LayerOutput:
        return LayerOutput(lhs * rhs, GradState(lhs, rhs), additional_info=dict())

    @jit
    def __activation_backward__(self,
                                gradient: jnp.ndarray,
                                grad_state: GradState) -> Tuple[jnp.ndarray, jnp.ndarray]:
        return gradient * grad_state.rhs, gradient * grad_state.lhs
