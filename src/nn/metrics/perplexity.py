
import jax.numpy as jnp

from src.utils.utils import softmax, sparse_crossentropy_loss

from .metric import Metric, MetricState

from jax import jit
from jax.tree_util import register_pytree_node_class


class State(MetricState):
    loss: int
    count: int

    def evaluate(self):
        return jnp.power(self.loss / self.count, 2)


@register_pytree_node_class
class Perplexity(Metric):

    def __init__(self,
                 is_probability_distribution: bool = False,
                 axis=-1,
                 sum_axis=-1,
                 name: str = "Perplexity"):
        """
        :param is_probability_distribution: If False, then compute softmax on predictions
        :param sum_axis: The axis to perform sum over elements. For example, in the RNN we want to minimize
                         sum of the sparse cross entropy loss across sequence. Therefore, we want to perform
                         double sum over the sequence and over distribution
        :param name: The name of the metric

        """
        super(Perplexity, self).__init__(name=name)

        self.is_probability_distribution = is_probability_distribution
        self.axis = axis
        self.sum_axis = sum_axis

    @staticmethod
    def new_state() -> State:
        return State(0, 0)

    @jit
    def forward(self, predictions, targets, state: State) -> State:
        """
        Computes perplexity between predictions and targets

        :param predictions: The predictions of the model in shape (n, m), where n is number of elements in batch and
                            m is probability distribution of m labels
        :param targets: The targets of the model in shape (n, m), where n is number of elements in batch and
                        m is probability distribution of m labels
        :param state: The state of the metric
        :return:
        """
        if not self.is_probability_distribution:
            predictions = softmax(predictions, axis=self.axis)

        loss = sparse_crossentropy_loss(predictions, targets, base=2, axis=self.sum_axis)

        return State(state.loss + loss.sum(), state.count + loss.size)
