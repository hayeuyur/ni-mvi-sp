
import jax.numpy as jnp

from src.utils.utils import softmax
from .metric import Metric, MetricState

from jax.tree_util import register_pytree_node_class
from jax import jit


class State(MetricState):
    correct: int
    count: int

    def evaluate(self):
        return self.correct / self.count


@register_pytree_node_class
class CategoricalAccuracy(Metric):

    def __init__(self,
                 is_probability_distribution: bool = False,
                 axis=-1,
                 name: str = "Categorical Accuracy"):
        """
        :param is_probability_distribution: If False, then compute softmax on predictions
        :param axis: The axis of probability distribution
        :param name: Name of the loss
        """
        super(CategoricalAccuracy, self).__init__(name=name)

        self.is_probability_distribution = is_probability_distribution
        self.axis = axis

    @staticmethod
    def new_state() -> State:
        return State(0, 0)

    @jit
    def forward(self, predictions, targets, state) -> State:
        if not self.is_probability_distribution:
            predictions = softmax(predictions)

        predictions = jnp.argmax(predictions, axis=self.axis)
        targets = jnp.argmax(targets, axis=self.axis)

        return State(
            correct=state.correct + (predictions == targets).sum(),
            count=state.count + targets.reshape(-1).shape[0]
        )
