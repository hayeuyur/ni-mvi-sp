from jax import jit
from jax.tree_util import register_pytree_node_class

from .clip import Clip


@register_pytree_node_class
class NoClip(Clip):

    @jit
    def __clip__(self, gradient):
        return gradient
