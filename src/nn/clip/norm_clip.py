from jax import jit
import jax.numpy as jnp
from jax.tree_util import register_pytree_node_class

from .clip import Clip


@register_pytree_node_class
class NormClip(Clip):

    def __init__(self, max_norm: float = 1, epsilon: float = 10e-9):
        super(NormClip, self).__init__()

        self.max_norm = max_norm
        self.epsilon = epsilon

    @jit
    def __clip__(self, gradient):
        norm = jnp.vdot(gradient, gradient)

        return jnp.where(norm < self.max_norm, gradient, gradient * (self.max_norm / (norm + self.epsilon)))
