from abc import ABC, abstractmethod
from jax.tree_util import tree_map
from jax import jit

from src.utils.jit import PytreeObject


class Clip(PytreeObject, ABC):

    def clip(self, gradients):
        return tree_map(self.__clip__, gradients)

    @abstractmethod
    @jit
    def __clip__(self, gradient):
        pass

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
