from jax import jit
import jax.numpy as jnp
from jax.tree_util import register_pytree_node_class

from .clip import Clip


@register_pytree_node_class
class ValueClip(Clip):

    def __init__(self, min: float = -1, max: float = 1):
        super(ValueClip, self).__init__()

        self.min = min
        self.max = max

    @jit
    def __clip__(self, gradient):
        return jnp.clip(gradient, a_min=self.min, a_max=self.max)
