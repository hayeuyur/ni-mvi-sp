import jax.numpy as jnp
import jax.tree_util
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput
# from src.nn.optimizers.optimizer import SparseGrad


class State(LayerState):
    embeddings: jnp.ndarray


class GradState(LayerGradState):
    inputs: jnp.ndarray


@register_pytree_node_class
class Embedding(Layer):

    def __init__(self, units: int, output_shape: int):
        super(Embedding, self).__init__()

        self.units = units
        self.output_shape = output_shape

    def new_state(self):
        return State(jnp.zeros((self.units, self.output_shape)))

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: State):
        embeddings = state.embeddings.take(inputs.input, axis=0)

        return LayerOutput(embeddings, GradState(inputs.input), dict())

    @jit
    def __backward__(self, gradient: LayerInput, state: State, grad_state: GradState):
        embeddings = jnp.zeros(state.embeddings.shape).at[grad_state.inputs, :].set(gradient.input)

        return LayerOutput(gradient.input,
                           SparseGrad(jax.tree_util.tree_map(lambda x: x.shape, state),
                                      State(embeddings),
                                      grad_state.inputs),
                           additional_info=gradient.additional_info)

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
