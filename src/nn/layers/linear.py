import jax.numpy as jnp
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput
from src.nn.initializers import Constant, override_initializer

from src.nn.operators.unary_operator import UnaryOperator, OperatorGradState


class State(LayerState):
    weight: jnp.ndarray
    bias: jnp.ndarray

    def get_params(self):
        return self.weight, self.bias


class GradState(LayerGradState):
    inputs: jnp.ndarray
    activation_state: OperatorGradState


@register_pytree_node_class
class Linear(Layer):

    def __init__(self,
                 input_shape: int,
                 output_shape: int,
                 activation: UnaryOperator):
        super(Linear, self).__init__()

        self.input_shape = input_shape
        self.output_shape = output_shape

        self.activation = activation

    def new_state(self):
        return State(jnp.zeros((self.input_shape, self.output_shape)),
                     override_initializer(Constant, jnp.zeros((self.output_shape,)), value=0))

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: State):
        hidden = jnp.dot(inputs.input, state.weight) + state.bias
        output = self.activation.forward(hidden)

        return LayerOutput(output.out, GradState(inputs.input, output.grad_state), dict())

    @jit
    def __backward__(self, gradient: LayerInput, state: State, grad_state: GradState):
        bias_grad = self.activation.backward(gradient.input, grad_state.activation_state)

        # Compute outer product of the weight gradient
        #
        # Let's imagine a simple dense layer, which outputs single value (shape (1,)).
        # In the backward pass, each neuron must receive the gradient of the output shape.
        # The scaling to provide more outputs is done by stacking single-output dense layer
        # For each value, we need a separate vector of weights, which we stack horizontally.
        # Therefore, the shape is (10,2), which means, that in each column we have a separate
        # set of weights for each output.
        # Now, in backward pass the layer receives the gradient of shape (1, 2) and we just need
        # to navigate each value to the column of weight matrix.
        # To do so, we calculate outer product using broadcasting trick.
        # We want to multiply  [k, 1] and [1, n] shapes, which will result into [k, n] shape.
        # It can be interpreted, as multiplication of vertical vector of shape (k) by each value in the vector (n)
        # and stacking the results horizontally.
        #
        # Note, that the same action can be performed in the following way.
        #   self._inputs.T @ bias_grad
        # However, you need to rescale the gradient, because the result is the sum of all gradients across batch
        weight_grad = grad_state.inputs[:, jnp.newaxis] * bias_grad[jnp.newaxis, :]
        output_grad = bias_grad @ state.weight.T

        return LayerOutput(output_grad, State(weight_grad, bias_grad), additional_info=dict())

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
