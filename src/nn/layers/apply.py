from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput

from src.nn.operators.unary_operator import OperatorGradState


class State(LayerState):
    pass


class GradState(LayerGradState):
    activation_state: OperatorGradState


@register_pytree_node_class
class Apply(Layer):
    """
    Stateless layer to perform operations
    """

    def __init__(self, activation):
        super(Apply, self).__init__()

        self.activation = activation

    def new_state(self):
        return State()

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: State):
        out = self.activation.forward(inputs.input)

        return LayerOutput(out.out, GradState(out.grad_state), inputs.additional_info)

    @jit
    def __backward__(self, gradient: LayerInput, state: State, grad_state: GradState):
        out = self.activation.backward(gradient.input, grad_state.activation_state)

        return LayerOutput(out, State(), additional_info=gradient.additional_info)

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
