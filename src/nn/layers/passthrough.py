
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput


class State(LayerState):
    pass


class GradState(LayerGradState):
    pass


@register_pytree_node_class
class Passthrough(Layer):

    def __init__(self):
        super(Passthrough, self).__init__()

    def new_state(self, params=None):
        return State()

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: State) -> LayerOutput:
        return LayerOutput(inputs.input, grad_state=GradState(), additional_info=dict())

    @jit
    def __backward__(self,
                     gradient: LayerInput,
                     state: LayerState,
                     grad_state: LayerGradState) -> LayerOutput:
        return LayerOutput(gradient.input, State(), additional_info=gradient.additional_info)

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
