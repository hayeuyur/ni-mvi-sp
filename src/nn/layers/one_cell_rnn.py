import jax.lax
from jax import jit
from jax.tree_util import register_pytree_node_class, tree_map

from src.nn.cells.cell import activation_key

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput

from functools import partial


class State(LayerState):
    cell_state: LayerState


class GradState(LayerGradState):
    cell_state: LayerGradState


@register_pytree_node_class
class OneCellRNN(Layer):

    def __init__(self, cell: Layer, return_states: bool = False):
        super(OneCellRNN, self).__init__()

        self.cell = cell
        self.return_states = return_states

    def new_state(self):
        return State(self.cell.new_state())

    @partial(jit, static_argnames=("grad",))
    def __forward__(self, inputs: LayerInput, grad: bool, state: State):
        result = self.cell.forward(inputs, grad=grad, state=state.cell_state)

        if self.return_states:
            return result

        return LayerOutput(result.out, GradState(result.grad_state), additional_info=dict())

    @jit
    def __backward__(self, gradient: LayerInput, state: State, grad_state: GradState):
        output_activation_gradient = gradient.additional_info.get(activation_key)

        cell_input = LayerInput(gradient.input, additional_info=dict(activation=output_activation_gradient))
        output = self.cell.backward(cell_input, state.cell_state, grad_state.cell_state)

        input_activation_gradient = output.additional_info.get(activation_key)
        grads = State(output.grad_state)

        info = dict(activation=input_activation_gradient) if self.return_states else dict()

        return LayerOutput(output.out, grads, info)

    def tree_flatten(self):
        children = (self.cell,)
        aux_data = {"return_states": self.return_states}

        return children, aux_data
