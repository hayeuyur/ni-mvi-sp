
from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput
from .passthrough import Passthrough


class State(LayerState):
    model_state: LayerState
    residual_connection_state: LayerState


class GradState(LayerGradState):
    model_state: LayerState
    residual_connection_state: LayerState


@register_pytree_node_class
class Residual(Layer):

    def __init__(self, model: Layer, residual_connection_model: Layer = Passthrough()):
        super(Residual, self).__init__()

        self.model = model
        self.residual_connection_model = residual_connection_model

    def new_state(self):
        return State(self.model.new_state(), self.residual_connection_model.new_state())

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: State) -> LayerOutput:
        model_out = self.model.forward(inputs, grad, state=state.model_state)
        residual_out = self.residual_connection_model.forward(inputs, grad, state=state.residual_connection_state)

        return LayerOutput(model_out.out + residual_out.out,
                           grad_state=GradState(model_out.grad_state, residual_out.grad_state),
                           additional_info=model_out.additional_info | residual_out.additional_info)

    @jit
    def __backward__(self,
                     gradient: LayerInput,
                     state: State,
                     grad_state: GradState) -> LayerOutput:
        model_output = self.model.backward(gradient, state.model_state, grad_state.model_state)
        residual_output = self.residual_connection_model.backward(
            gradient, state.residual_connection_state, grad_state.residual_connection_state
        )

        return LayerOutput(model_output.out + residual_output.out,
                           State(model_output.grad_state, residual_output.grad_state),
                           additional_info=model_output.additional_info | residual_output.additional_info)

    def tree_flatten(self):
        children = (self.model, self.residual_connection_model)
        aux_data = {}

        return children, aux_data
