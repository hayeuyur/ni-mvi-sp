import jax
import jax.numpy as jnp

from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput
from typing import Tuple
from functools import partial


class State(LayerState):
    pass


class GradState(LayerGradState):
    pass


@register_pytree_node_class
class Reshape(Layer):
    def __init__(self, input_shape, output_shape):
        super(Reshape, self).__init__()

        self.input_shape = input_shape
        self.output_shape = output_shape

    def new_state(self):
        return State()

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: State):
        return LayerOutput(inputs.input.reshape(self.output_shape),
                           GradState(),
                           additional_info=inputs.additional_info)

    @jit
    def __backward__(self, gradient: LayerInput, state: State, grad_state: GradState):
        return LayerOutput(jnp.reshape(gradient.input, self.input_shape),
                           grad_state=State(),
                           additional_info=gradient.additional_info)

    def tree_flatten(self):
        children = ()
        aux_data = self.__dict__

        return children, aux_data
