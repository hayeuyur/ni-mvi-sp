
from abc import ABC, abstractmethod
from typing import NamedTuple, List, Union, Sequence, Any, Dict

import jax
from jax import jit
import jax.numpy as jnp

from src.utils.jit import PytreeObject

from functools import partial


LayerState = NamedTuple
LayerGradState = NamedTuple


class LayerInput(NamedTuple):
    input: jnp.ndarray
    additional_info: Dict[str, jnp.ndarray]


class LayerOutput(NamedTuple):
    out: jnp.ndarray
    grad_state: LayerGradState
    additional_info: Dict[str, jnp.ndarray]


class Layer(PytreeObject, ABC):

    def __init__(self):
        super(Layer, self).__init__()

    @abstractmethod
    def new_state(self):
        pass

    @partial(jit, static_argnums=(2,))
    def forward(self, inputs: LayerInput, grad: bool, state: LayerState) -> LayerOutput:
        """
        Implements single pass computation.
        To perform backward pass over batches use v_forward method.

        :param inputs: Inputs
        :param grad: If true, then will compute gradient, otherwise will return empty gradient state
        :param state: The state of the neural network
        :return:
        """
        return self.__forward__(inputs, grad, state)

    @partial(jit, static_argnames=("grad", "in_axes", "out_axes"))
    def vforward(self,
                 inputs: LayerInput,
                 grad: bool,
                 state: LayerState,
                 in_axes=0,
                 out_axes=0) -> LayerOutput:
        """
        A simple wrapper around jax vmap method

        :param inputs: Inputs in form of batch
        :param grad: If true, then will compute gradient
        :param state: The state of the layer
        :param in_axes: Input axes to perform traversal. See jax.vmap documentation
        :param out_axes: Output axes to perform traversal. See jax.vmap documentation
        :return:
        """
        return jax.vmap(lambda x: self.__forward__(x, grad, state), in_axes=in_axes, out_axes=out_axes)(inputs)

    @jit
    def backward(self,
                 gradient: LayerInput,
                 state: LayerState,
                 grad_state: LayerGradState) -> LayerOutput:
        """
        Implements single backward pass.
        To perform backward pass over batches use v_backward method.

        :param gradient: The gradient
        :param state: The state of the network
        :param grad_state: The state of the gradient
        :return:
        """
        return self.__backward__(gradient, state, grad_state)

    @partial(jit, static_argnames=["in_axes", "out_axes"])
    def vbackward(self,
                  gradient: LayerInput,
                  state: LayerState,
                  grad_state: List[LayerGradState],
                  in_axes: Union[int, Sequence[Any]] = (0, 0),
                  out_axes: Union[int, Sequence[Any]] = 0) -> LayerOutput:
        """
        A simple wrapper around jax.vmap method

        :param gradient: The gradient vector
        :param state: The state of the layer
        :param grad_state: The gradient state received from the forward pass
        :param in_axes: Input axes to perform traversal. See jax.vmap documentation
        :param out_axes: Output axes to perform traversal. See jax.vmap documentation
        :return:
        """
        return jax.vmap(
            lambda _gradient, _grad_state: self.__backward__(_gradient, state, _grad_state),
            in_axes=in_axes,
            out_axes=out_axes
        )(gradient, grad_state)

    @abstractmethod
    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: LayerState) -> LayerOutput:
        pass

    @abstractmethod
    @jit
    def __backward__(self,
                     gradient: LayerInput,
                     state: LayerState,
                     grad_state: LayerGradState) -> LayerOutput:
        pass
