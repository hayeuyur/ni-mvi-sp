import jax
import jax.numpy as jnp

from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput


class State(LayerState):
    layer_state: LayerState


class GradState(LayerGradState):
    layer_grad_state: LayerGradState


@register_pytree_node_class
class Broadcast(Layer):
    """
     A simple layer, which broadcasts the sequence through the layer and correctly computes the update state for the
     layer
     """

    def __init__(self, layer: Layer, axis: int = 0):
        super(Broadcast, self).__init__()

        self.layer = layer
        self.axis = axis

    def new_state(self):
        return State(layer_state=self.layer.new_state())

    @partial(jit, static_argnames="grad")
    def __forward__(self, inputs: LayerInput, grad: bool, state: State):
        out = self.layer.vforward(inputs,
                                  grad,
                                  state.layer_state,
                                  in_axes=self.axis)

        return LayerOutput(out.out, GradState(out.grad_state), additional_info=out.additional_info)

    @jit
    def __backward__(self, gradient: LayerInput, state: State, grad_state: GradState):
        out = self.layer.vbackward(gradient,
                                   state.layer_state,
                                   grad_state.layer_grad_state,
                                   out_axes=self.axis)

        def sum(x):
            return jnp.sum(x, axis=self.axis)

        layer_grad_state = jax.tree_util.tree_map(sum, out.grad_state)

        return LayerOutput(out.out, grad_state=State(layer_grad_state), additional_info=out.additional_info)

    def tree_flatten(self):
        children = (self.layer,)
        aux_data = {"axis": self.axis}

        return children, aux_data
