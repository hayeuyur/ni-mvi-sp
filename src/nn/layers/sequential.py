from jax import jit
from jax.tree_util import register_pytree_node_class

from functools import partial

from .layer import Layer, LayerState, LayerGradState, LayerInput, LayerOutput

from typing import List


class State(LayerState):
    states: List[LayerState]


class GradState(LayerState):
    states: List[LayerGradState]


@register_pytree_node_class
class Sequential(Layer):

    def __init__(self, layers):
        super(Sequential, self).__init__()

        self.layers = layers

    def new_state(self):
        return State([layer.new_state() for layer in self.layers])

    @partial(jit, static_argnames=("grad",))
    def __forward__(self, inputs: LayerInput, grad: bool, state: State):
        result = inputs
        new_grad_states = []

        # TODO: Integrate with jax.lax.scan
        for layer, _state in zip(self.layers, state.states):
            output = layer.forward(result, grad, _state)

            result = LayerInput(output.out, output.additional_info)
            new_grad_states += [output.grad_state]

        return LayerOutput(result.input, GradState(new_grad_states), dict())

    @jit
    def __backward__(self, gradient, state: State, grad_state: GradState):
        result = gradient
        grads = []

        # TODO: Integrate with jax.lax.scan
        for layer, _state, _grad_state in zip(reversed(self.layers),
                                              reversed(state.states),
                                              reversed(grad_state.states)):
            output = layer.__backward__(result, _state, _grad_state)

            result = LayerInput(output.out, additional_info=output.additional_info)
            grads += [output.grad_state]

        return LayerOutput(result.input, State(list(reversed(grads))), additional_info=result.additional_info)

    def tree_flatten(self):
        children = (self.layers,)
        aux_data = {}

        return children, aux_data
