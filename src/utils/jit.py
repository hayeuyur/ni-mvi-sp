from abc import ABC, abstractmethod


class PytreeObject(ABC):

    @abstractmethod
    def tree_flatten(self):
        pass

    @classmethod
    def tree_unflatten(cls, aux_data, children):
        return cls(*children, **aux_data)


class NoParameterPytreeObject(PytreeObject):

    def tree_flatten(self):
        children = ()
        aux_data = {}
        return children, aux_data
