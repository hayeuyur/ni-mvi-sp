import math

import jax.dlpack
import tensorflow as tf


def from_tensorflow_to_jax(x):
    dlpack = tf.experimental.dlpack.to_dlpack(x)

    return jax.dlpack.from_dlpack(dlpack)


def split_dataset(dataset, train, validation, test):
    assert train + validation + test <= 1, "Expect floating point ratios for the dataset"

    size = len(dataset)
    train_size = math.floor(train * size)
    validation_size = math.floor(validation * size)
    test_size = math.floor(test * size)

    train = dataset.take(train_size)
    validation = dataset.skip(train_size).take(validation_size)
    test = dataset.skip(train_size).skip(validation_size).take(test_size)

    return train, validation, test
