import jax.numpy as jnp
from jax import jit

from functools import partial


@partial(jit, static_argnums=(1,))
def softmax(x, axis=-1):
    x_max = jnp.max(x, axis, keepdims=True)
    exp = jnp.exp(x - x_max)

    return exp / (jnp.sum(exp, axis=axis, keepdims=True) + 10e-20)


@partial(jit, static_argnums=2)
def sparse_crossentropy_loss(predictions, targets, axis=1, base: float = jnp.e, epsilon: float = 1e-20):
    base = jnp.log(base)

    return jnp.sum(-targets * (jnp.log(predictions + epsilon) / base), axis=axis)


@partial(jit, static_argnums=(2, 3))
def mean_sparse_crossentropy_loss(predictions,
                                  targets,
                                  mean_axis=None,
                                  axis=1,
                                  base: float = jnp.e,
                                  epsilon: float = 1e-20):
    base = jnp.log(base)

    return jnp.mean(jnp.sum(-targets * jnp.log(predictions + epsilon) / base, axis=axis), axis=mean_axis)


@jit
def sparse_crossentropy_loss_gradient(predictions, targets):
    return predictions - targets
