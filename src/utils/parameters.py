import inspect

from collections import namedtuple
from typing import List


ParsedParameter = namedtuple('ParsedParameter', ['name', 'default', 'type'])


def get_parameters(method, include_self: bool = False) -> List[ParsedParameter]:
    signature = inspect.signature(method)

    parameters = []

    for parameter_id, parameter in signature.parameters.items():
        if parameter_id != 'self' or include_self:
            parameters += [ParsedParameter(
                name=parameter_id,
                default=None if parameter.default is parameter.empty else parameter.default,
                type=None if parameter.default is parameter.empty else parameter.annotation
            )]

    return parameters
