
import h5py

import jax.numpy as jnp
from jax.tree_util import PyTreeDef, tree_flatten, tree_unflatten

from .jit import PytreeObject


def write_tree(tree: PytreeObject, path: str):
    with h5py.File(path, 'w') as file:
        tree = tree_flatten(tree)[0]

        for index, value in enumerate(tree):
            file.create_dataset(data=value, name=f"leaf_{index:08d}")


def load_tree(path: str, tree_def):
    with h5py.File(path, 'r') as file:
        leaves = []

        for leaf in sorted(file.values(), key=lambda x: int(x.name.split('_')[1])):
            leaves += [jnp.array(leaf)]

        return tree_unflatten(tree_def, leaves)
