import math

import jax.numpy as jnp
import jax.random


def generate_indices(length, train, validation, test, key):
    key, subkey = jax.random.split(key)
    indices = jnp.arange(length)
    indices = jax.random.permutation(subkey, indices, independent=True)

    train_size = math.floor(train * length)
    validation_size = math.floor(validation * length)
    test_size = math.floor(test * length)

    return key,\
           indices[:train_size], \
           indices[train_size:train_size+validation_size], \
           indices[train_size + validation_size:]
