
import jax
from jax.tree_util import tree_map
import jax.numpy as jnp

from .jit import PytreeObject
from typing import Tuple


def tree_map_variables(func, tree: PytreeObject) -> PytreeObject:
    return tree_map(lambda x: func(x) if isinstance(x, jax.Array) else x, tree)


def split_tree(tree: PytreeObject,
               is_leaf=lambda x: len(x) == 2 and isinstance(x[0], jnp.ndarray)) -> Tuple[PytreeObject, PytreeObject]:
    """

    :param tree: The tree object to split
    :param is_leaf: The function, which must tell if the node is splittable. The default implementation consider,
                    the node as leaf, if the left children is jnp.ndarray
    :return:
    """
    first = tree_map(lambda _tree: _tree[0], tree, is_leaf=is_leaf)
    second = tree_map(lambda _tree: _tree[1], tree, is_leaf=is_leaf)

    return first, second

